### Data for residential district "Am Oelper Berge" in Brunswick, Lower Saxony, Germany

This data set represents the residential district “Am Oelper Berge”, an existing residential 
district in Brunswick, Lower Saxony in Germany and its unregulated low-voltage grid. The grid 
delivers electricity to 49 multi-apartment buildings, each including six households on average. 
A total number of 1152 residents is assumed. The household loads are calculated using generic
open-source data of measured load profiles for the year 2019 from [1]. The district includes 144 
parking spaces on public grounds, allowing this number to be integrated as possible EV charging
points in the future. The maximum rated power of a charging station in the district is set to
11 kW. The EV’s electricity demand is derived from emobpy profiles, which uses mobility data from 
the German mobility survey “Mobiltät in Deutschland” from 2017 [2]. The mobility behavior is 
generated randomly but assumed to be composed chiefly of 62% commuters 
(78% full-time and 22% part-time), as stated in [2]. Moreover, a specific BEV model
is assigned in each profile. The BEV models used in this work are based on a data evaluation of the 
top 10 BEV registrations in Germany with adjustment to the assumed predominantly low to medium 
economic status of the residents [4]. The meteorological data for the PV system models was 
extracted from the German weather service (DWD) public available data [5]. The
downloading and processing of the required meteorological data was executed using a self-developed 
open-source package and the results were saved to csv-files [6]. The sizing of the PV systems
and battery storage systems, respectively their nominal power or capacity, is set in accordance 
with the corresponding building’s total energy consumption (1 kWp/1 kWh per 1 MWh) [5]. 

For detailed information see [6].


#### References

[1]: Beyertt, A., Verwiebe, P., Seim, S., et al.: ‘Felduntersuchung
zu behavioral energy efficiency Potentialen
von privaten Haushalten’, Working Paper, 2020,
https://doi.org/10.5281/zenodo.3855575
[2]: Gaete.Morales, C., Kramer, H., Schill, W.P., et al.: ‘An
open tool for creating battery-electric vehicle time series
from empirical data, emobpy’, Scientific data, 2021, 8,
(1), pp. 152
[3]: Fayed, S., Penaherrera Vaca, F., Wagner, H. & Rolink,
J. An Open Source Grid Observer for the Analysis of
Power Flexibilities in Low Voltage Distribution Grid
Simulations: 10th International Conference on Smart
Grid and Clean Energy Technologies (ICSGCE): 14.10-
16.10.2022.
[4]: DWD: ‘DWD’s open data server’, 2022, https://opendata.dwd.de/ Penaherrera Vaca, F.A.
[5]: Penaherrera Vaca, F.A.. ‘ZLE DWD data downloader’,
https://gitlab.com/zdin-zle/models/dwd-data-downloader
[6]: Wagner, H., Peñaherrera V., F., Fayed, S., Werth, O., Eckhoff, S.: Co-simulation-based 
analysis of the grid capacity for electric vehicles in districts: the case of “Am Ölper Berge” in 
Lower Saxony, 6th E-Mobility Power System Integration Symposium, EMOB 2022, The Hague, Netherlands, 
IET Digital Library, 10. Oktober 2022, S. 33-41. ISBN 978-1-83953-832-2. DOI: 10.1049/icp.2022.2713