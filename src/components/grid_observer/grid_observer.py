"""
@author: Fernando Penaherrera @UOL/OFFIS

This class has the function of evaluating if the grid has voltage level issues.
It checks voltage levels across the grid and sends a status report

Integrated by Henrik Wagner @elenia/TUBS.

"""
import copy

OUTPUT_ATTRS = {"Bus_P_ing": ["p_mw"]}


class GridObserver:
    """Pandapower Grid Observer"""

    # results properties
    results = ["status", "bus_violation"]
    net = None

    def __init__(self, init_vals):
        """Initializer

        Args:
            init_vals (dict): Dictionary with initial values
        """

        # assign initial values
        if init_vals is not None:
            for _, (key, value) in enumerate(init_vals.items()):
                setattr(self, key, value)

    def step(self, time, grid):
        """Advance the model

        Args:
            time (int): Size of the time step
            grid (Mosaik.net): Net object created by MOSAIK. Contains a pandapower.net class
        """

        # reset Grid Violation Status
        self.status = True
        self.bus_violation = {}

        # deepcopy to avoid problems with instances
        self.net = copy.deepcopy(grid.sim._inst.simulator.net)
        violations = []

        # new dict only with buses
        for bus in self.net.res_bus.index:
            v_val = self.net.res_bus.at[bus, "vm_pu"]
            if not 0.95 <= v_val <= 1.05:
                violations.append(v_val)
                self.bus_violation[bus] = v_val
        if len(violations) > 0:
            self.status = False

            # TODO pass this prints if the verbose is true.

            # print(f"Time: {time}, Voltage violations in buses: ")
            # print(self.bus_violation)


class Simulator(object):
    """Simulates a number of ``Model`` models and collects some data.

    Args:
        object (Class): generic class object
    """

    # contructor

    def __init__(self):
        """Initializer"""
        # init data elments
        self.models = []
        self.results = []

    # create model instances
    def add_model(self, init_vals=None):
        """Adds a model of the grid observer

        Args:
            init_vals (dict, optional): Initial values of the model. Defaults to None.

        Returns:
            GridObserver: A model of the Grid Observer
        """

        # create grid_observer model
        model = GridObserver(init_vals)

        # add model to model grid_observer
        self.models.append(model)

        # add an empty list for simulation data results saving
        self.results.append([])

        # return model
        return model

    # perform simulation step of all models
    def step(self, time):
        """Advances a step for every model

        Args:
            time (int): Size of the time step
        """

        # enumeration over all models in simulator
        for i, model in enumerate(self.models):

            # perform simulation step
            model.step(time, self.grid)

            # collect data of model and storage local
            for _, signal in enumerate(model.results):
                self.results[i].append(getattr(model, signal))

    def add_controlled_buses(self, buses):
        """Adds a list of controlled buses

        Args:
            buses (list): list of buses
        """
        self.buses = buses

    def add_controlled_grid(self, grid):
        """Adds a controlled grid

        Args:
            grid (pandapower.net): A pandapower Net object describing a grid
        """
        self.grid = grid
