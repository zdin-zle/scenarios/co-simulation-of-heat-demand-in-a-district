"""
@author: Christian Reinhold and Henrik Wagner elenia@TUBS

Model of an electrical battery storage including dynamic efficiency 
curves of battery inverter
The electrical storage model mosaik-storage was built to
replicate the charging and discharging processes of 
lithium-ion battery storage systems.This includes internal 
calculations of state of charge (SOC), charging and discharging efficiency 
depending on the utilization (replicated based on measured efficiency 
curves of battery inverters), available charging and discharging power, 
and state of health (SOH).

"""

# import packages
import decimal
from decimal import Decimal


class eStorage:
    """Battery Storage Model: mosaik-storage"""

    # Static properties - overwritten by model_data.json
    type = "eStorage"  # Model type [-]
    soc_init = 0.5  # Initial SOC [-]
    E_bat_rated = 10000  # Capacity [Wh]
    P_rated_discharge_max = 8000  # Rated discharging active power [W]
    P_rated_charge_max = 8000  # Rated charging active power [W]
    eff_discharge_static = 0.95  # static efficiency discharge [-]
    eff_charge_static = 0.95  # static efficiency charge [-]
    status_curve = False  # on/off dynamic charging [-]
    loss_rate = 0.02  # self-discharge per month [%/month]
    DOD_max = 0.95  # max. depth of discharge [-]
    status_aging = False  # on/off consideration of reduced battery
    # capacity over time (battery aging effects)
    SoH_init = 1  # Initial SoH at t=0, 1 equals 100%
    SoH_cycles_max = 0.8  # Cycle limit of SoH, at 80% mostly battery
    # turns into 2nd life
    bat_cycles_max_soh = 0.8
    bat_cycles_max = 5000  # Max. battery cycles [-]
    bat_cycles_init = 0  # Initial battery cycles storage [-]

    # Battery operation strategies
    operation_strategy = "uncontrolled"  # Default strategy: uncontrolled charging

    # Parameter of operation strategies
    charging_limit = (
        "None"  # Percentage to which battery charging power is reduced to avoid curtailment of PV
    )

    # Simulation properties
    delta = 60  # Time-Delta [s]

    # Dynamic input properties
    P_SET = None  # Set-Point - Battery Active Power
    # (+ Charge, - Discharge) [W]

    # Dynamic output properties
    E_BAT = 0  # Current charging level [Wh]
    P_SET_LIMIT = None  # Limitation of Set Point [W]
    SOC = None  # Current SOC - State of Charge [-]
    P_CHARGE_MAX = None  # Current maximal active charge power [W]
    P_DISCHARGE_MAX = None  # Current maximal active discharge power [W] .
    BAT_CYCLES = None  # Current amount of battery cycles [-]
    SOH = None  # Current state of health of battery [-]
    P = None  # Current charging power at grid node [W]
    P_FLEX_CHARGE_MAX = None  # Available Maximum charging power for flexibility use
    P_FLEX_DISCHARGE_MAX = None  # Available Maximum discharging power for flexibility use

    # result properties
    results = [
        "E_BAT",
        "P_SET_LIMIT",
        "SOC",
        "P_CHARGE_MAX",
        "P_DISCHARGE_MAX",
        "P_FLEX_CHARGE_MAX",
        "P_FLEX_DISCHARGE_MAX",
        "BAT_CYCLES",
        "SOH",
    ]

    def __init__(self, init_vals=None):
        """Class Constructor. Uses the the static properties as entry.

        Args:
            init_vals (dict, optional): Dictionary containing the initial values. Defaults to None.
        """

        # assign initial values from the init_vals dictionary
        if init_vals:
            for key, value in init_vals.items():
                setattr(self, key, value)

        # set actual SOC
        self.SOC = self.soc_init

        # calculate usable battery capacity
        self.E_bat_usable = self.E_bat_rated * self.DOD_max

        # set real charging level E-Bat
        self.E_BAT = self.SOC * self.E_bat_usable

        # calculate energy of one charge-discharge cycle
        self.E_cycle = 2 * self.E_bat_usable

        # set amount of battery cycles
        self.BAT_CYCLES = self.bat_cycles_init

        # set initial SoH
        self.SOH = self.SoH_init

        # set initial set point for active battery power value
        self.P_SET = 0

        # set initial charge/discharge efficiency
        decimal.getcontext().prec = 4  # set the precision digits to two digits
        self.eta_charge = self.eff_charge_static
        self.eta_discharge = self.eff_discharge_static

        # calculate self-discharge per timestep
        self.self_discharge = (self.loss_rate / (30 * 24 * 60 * 60)) * self.delta

    def set_active_power_limit(self):
        """
        Sets the output active power.
        Controls that it does not exceed the battery charge
        and discharge limits
        """
        if self.operation_strategy == "uncontrolled":
            self.uncontrolled()

        elif self.operation_strategy == "minimize_curtailment":
            self.minimize_curtailment()

    def minimize_curtailment(self):
        """
        This strategy tries to minimize the curtailment of the solar power system
        by reducing the charging power. This decreases self-sufficiency but also the
        curtailment due to EEG2021 (e.g. feed-in is only allowed at max. 70% of
        installed power for uncontrolled solar power systems). This operation strategy
        should not be used in combination with electric vehicle charging. Suitable limits
        are 30%-60%.

        """
        # check if charging limit is set
        if self.charging_limit == None:
            # set default limit of 60%
            self.charging_limit = 0.6
        # Set Limit considering charging limits to reduce curtailment
        if self.P_SET > (self.P_rated_charge_max * self.charging_limit):
            self.P_SET_LIMIT = self.P_rated_charge_max * self.charging_limit
        elif self.P_SET < -self.P_rated_discharge_max:
            self.P_SET_LIMIT = -self.P_rated_discharge_max
        else:
            self.P_SET_LIMIT = self.P_SET

    def uncontrolled(self):
        """
        Standard operation strategy for battery storage system.
        """
        # Set Limit
        if self.P_SET > self.P_rated_charge_max:
            self.P_SET_LIMIT = self.P_rated_charge_max
        elif self.P_SET < -self.P_rated_discharge_max:
            self.P_SET_LIMIT = -self.P_rated_discharge_max
        else:
            self.P_SET_LIMIT = self.P_SET

    def calculate_new_charging_level(self, time):
        """Calculates the updated energy content after charging/discharging using the
        charging efficiency levels (eta_charge / eta_discharge).

        Args:
            time (int): Current simulation time
        """
        # reset turnover/volume of energy per step
        self.E_bat_step_volume = 0

        # CASE: Charging of battery storage
        if self.P_SET_LIMIT >= 0:
            self.calc_eta_charge()
            overcharging_signal = False

            # calculate new energy volume of storage
            if self.E_BAT < self.E_bat_usable:
                self.E_bat_step_volume = self.P_SET_LIMIT * (self.delta / 3600) * self.eta_charge
                self.E_BAT = self.E_BAT + self.E_bat_step_volume

                # Check that battery energy content is not surpassed
                if self.E_BAT > self.E_bat_usable:
                    overcharging_signal = True
                    E_overcharged = self.E_BAT - self.E_bat_usable
                    self.E_BAT = self.E_bat_usable

            # set power value of storage for grid calculation
            if self.E_bat_step_volume == 0:
                self.P = 0
            elif overcharging_signal == True:
                if self.eta_discharge > 0:
                    self.P = (
                        min(self.E_bat_step_volume - E_overcharged)
                        / self.eta_discharge
                        / (self.delta / 3600)
                    )
                else:
                    # Checking for malfunction of storage model
                    print(f"Temporary error at {time}")
                    self.P = self.P_SET_LIMIT
            else:
                self.P = self.P_SET_LIMIT

            # Checking for malfunction of storage model
            if self.P > self.P_SET_LIMIT:
                print(f"Temporary error at {time}")
                self.P = self.P_SET_LIMIT

        # CASE: Discharging of battery storage
        elif self.P_SET_LIMIT < 0:
            self.calc_eta_discharge()  # calculate dynamic discharging efficiency
            undercharging_signal = False

            if self.E_BAT > 0:
                self.E_bat_step_volume = self.P_SET_LIMIT * self.delta / 3600 * self.eta_discharge
                self.E_BAT = self.E_BAT + self.E_bat_step_volume

                # Check if battery level drops under 0
                if self.E_BAT < 0:
                    E_undercharged = abs(self.E_BAT)
                    self.E_BAT = 0

            # set power value of storage for grid calculation
            if self.E_BAT == 0:
                self.P = 0
            elif self.E_bat_step_volume == 0:
                self.P = 0
            elif undercharging_signal == True:
                self.P = (
                    (self.E_bat_step_volume - E_undercharged)
                    / self.eta_discharge
                    / (self.delta / 3600)
                )
            else:
                self.P = self.P_SET_LIMIT

    def calc_P_FLEX(self):
        """Calculate available charging/discharging power before step with static efficiency"""
        # Available maximum charging power before step with static efficiency
        if self.P_FLEX_CHARGE_MAX == None:
            self.P_FLEX_CHARGE_MAX = min(
                (self.E_bat_usable - self.E_BAT) / self.eff_charge_static / (self.delta / 3600),
                self.P_rated_charge_max,
            )
        else:
            self.P_FLEX_CHARGE_MAX = self.P_CHARGE_MAX

        # Available maximum discharging power before the step with static efficiency
        if self.P_FLEX_DISCHARGE_MAX == None:
            self.P_FLEX_DISCHARGE_MAX = min(
                (self.E_bat_usable - self.E_BAT) / self.eff_discharge_static / (self.delta / 3600),
                self.P_rated_discharge_max,
            )

        else:
            self.P_FLEX_DISCHARGE_MAX = self.P_DISCHARGE_MAX

    def calc_P_DISCHARGE_MAX(self):
        """Calculates maximum discharging active power.
        Note: P_DISCHARGE_MAX cannot be 0 as control then never initiates a charging/discharging process.
        Therefore, in times with low P_SET we set P_DISCHARGE_MAX higher to show the potential."""
        # Calculate potential for maximum discharge for times with low P_ratio
        if self.eta_discharge == 0 and self.P_ratio == 0:
            self.P_DISCHARGE_MAX = min(
                (self.E_bat_usable - self.E_BAT) / self.eta_discharge_max / (self.delta / 3600),
                self.P_rated_discharge_max,
            )

        # Calculate maximum discharging active ower
        else:
            self.P_DISCHARGE_MAX = min(
                self.E_BAT * self.eta_discharge / (self.delta / 3600), self.P_rated_discharge_max
            )

    def calc_P_CHARGE_MAX(self):
        """Calculates the maximum charging active power of the batter storage.
        Note: P_CHARGE_MAX should only be 0 when P_ratio is very low so that inverter does not turn on"""
        # calculate potential maximum charging power for times with low P_ratio
        if (self.eta_charge == 0 and self.P_ratio == 0) or (
            self.eta_charge == 0 and self.P_SET_LIMIT <= 0
        ):
            self.P_CHARGE_MAX = min(
                (self.E_bat_usable - self.E_BAT) / self.eta_charge_max / (self.delta / 3600),
                self.P_rated_charge_max,
            )

        # should be 0 when P_ratio is low as inverter does not run then
        elif self.eta_charge == 0 and self.P_ratio <= Decimal(0.012):
            self.P_CHARGE_MAX = 0

        else:
            self.P_CHARGE_MAX = min(
                (self.E_bat_usable - self.E_BAT) / self.eta_charge / (self.delta / 3600),
                self.P_rated_charge_max,
            )

        # Calculate curtailed charging active power
        if self.operation_strategy == "minimize_curtailment":
            if (self.eta_charge == 0 and self.P_ratio == 0) or (
                self.eta_charge == 0 and self.P_SET_LIMIT <= 0
            ):
                self.P_CHARGE_MAX = min(
                    (self.E_bat_usable - self.E_BAT) / self.eta_charge_max / (self.delta / 3600),
                    self.P_rated_charge_max * self.charging_limit,
                )

            elif self.eta_charge == 0 and self.P_ratio <= Decimal(0.012):
                self.P_CHARGE_MAX = 0
            else:
                self.P_CHARGE_MAX = min(
                    (self.E_bat_usable - self.E_BAT) / self.eta_charge / (self.delta / 3600),
                    self.P_rated_charge_max * self.charging_limit,
                )

    def calculate_aging_status(self):
        """Checks if the State of Health (SoH) limit has been reached.

        Raises:
            ValueError: Raises warning when minimum SoH of battery storage has been reached.
        """

        # Check if SoH limit is already reached
        if self.SOH > (1 - self.SoH_cycles_max):

            # Calculate bat_aging per cycle and new SoH
            self.bat_aging = (self.SoH_cycles_max - self.SoH_init) / (
                self.bat_cycles_max - self.BAT_CYCLES
            )
            self.SOH = self.SoH_init + self.bat_aging * self.BAT_CYCLES

            # calculate reduced E_bat_usable due to aging
            self.E_bat_usable = (self.E_bat_rated * self.DOD_max) * self.SOH

        elif self.SoH <= (1 - self.SoH_cycles_max):
            raise ValueError(
                "Warning: Minimum SoH of %s has been reached - battery needs to be replaced or SoH limit / bat_cycles_init \
                to be increased"
                % (1 - self.SoH_cycles_max)
            )

    def calc_eta_discharge(self):
        """
        Calculation of dynamic discharging efficiency (eta_BAT2AC)
        """

        if self.status_curve:
            self.eta_BAT2AC = [Decimal(a) for a in self.eta_BAT2AC]

            self.P_ratio = decimal.Decimal(abs(self.P_SET_LIMIT / self.P_rated_discharge_max))

            if self.P_ratio <= Decimal(
                0.012
            ):  # note: battery inverter does not start for low P_ratio
                self.eta_discharge = 0
            else:
                # Formula:
                # eta = a0 * (P/P_max)^a1 + a2
                self.eta_discharge = float(
                    self.eta_BAT2AC[0] * (self.P_ratio ** self.eta_BAT2AC[1]) + self.eta_BAT2AC[2]
                )

            # calc eta_discharge_max = eta_discharge for P_rated_discharge_max
            self.eta_discharge_max = float(
                self.eta_BAT2AC[0] * (1 ** self.eta_BAT2AC[1]) + self.eta_BAT2AC[2]
            )

        # case static efficiency: set maximum discharging efficiency to static value
        self.eta_discharge_max = self.eff_discharge_static

    def calc_eta_charge(self):
        """
        Calculation of dynamic charging efficiency (eta_AC2BAT)
        """
        if self.status_curve:
            self.eta_AC2BAT = [Decimal(a) for a in self.eta_AC2BAT]

            self.P_ratio = Decimal(abs(self.P_SET_LIMIT / self.P_rated_charge_max))
            if self.P_ratio <= Decimal(
                0.012
            ):  # note: battery inverter does not start for low P_ratio
                self.eta_charge = 0
            else:
                # Formula:
                # eta = a0 * (P/P_max)^a1 + a2
                self.eta_charge = float(
                    self.eta_AC2BAT[0] * (self.P_ratio ** self.eta_AC2BAT[1]) + self.eta_AC2BAT[2]
                )
            # calc eta_discharge_max = eta_discharge for P_rated_discharge_max
            self.eta_charge_max = float(
                self.eta_BAT2AC[0] * (1 ** self.eta_BAT2AC[1]) + self.eta_BAT2AC[2]
            )

        # case static efficiency: set maximum charging efficiency to static value
        self.eta_charge_max = self.eff_charge_static

    def __repr__(self):
        """String for identification of the battery"""

        text = "Electric Battery Storage Model" + "\n"
        text += "Capacity [Wh] = %s" % (self.E_bat_rated) + "\n"
        text += "Real Capacity [Wh] = %s" % (self.E_bat_usable) + "\n"
        text += "Maximum Charging Power [W] = %s" % (self.P_CHARGE_MAX) + "\n"
        text += "Maximum Discharging Power [W] = %s" % (self.P_DISCHARGE_MAX) + "\n"
        return text

    def step(self, time):
        """
        Advances the battery storage model.
        Calculates all of the dynamic properties based on the static properties.

        Args:
            time (int): Current simulation time

        """

        # Calculate available charging/discharging power before step with static efficiency
        self.calc_P_FLEX()

        # Set active power limit
        self.set_active_power_limit()

        # Calculate energy loss due to self-discharge
        self.E_BAT = self.E_BAT * (1 - self.self_discharge)

        # Calculate resulting power flow at grid node P and new energy level E_BAT
        self.calculate_new_charging_level(time)

        # Calculate SOC
        self.SOC = min(self.E_BAT / self.E_bat_usable, 1)

        # Calculate battery cycles
        self.BAT_CYCLES = self.BAT_CYCLES + abs(self.E_bat_step_volume / self.E_cycle)

        # Calculate maximum discharging active power
        self.calc_P_DISCHARGE_MAX()

        # Calculate maximum charging active power
        self.calc_P_CHARGE_MAX()

        # Calculate battery state of health (SoH) and aging properties
        if self.status_aging:
            self.calculate_aging_status()


class Simulator(object):
    """Contains a list with Storage Models and a result list
    Used to access information on the different entities of the model

    Args:
        object (class): Generic class inheritance

    """

    # Constructor
    def __init__(self):
        """Constructor"""
        # init data elements
        self.models = []
        self.results = []

    # create model instances
    def add_model(self, etype="eStorage", init_vals=None):
        """_summary_

        Args:
            etype (str, optional): _description_. Defaults to 'eStorage'.
            init_vals (dict, optional): Dictionary with the initial values.
                It must contain the necessary battery parameters e.g.
                "E_bat_rated", "status_curve", "operation_strategy":
                        init_vals= {"E_bat_rated": 10000,
                                    "status_curve": false,
                                    "operation_strategy": "uncontrolled
                                    }

        Returns:
            _type_: _description_
        """

        # select by type
        if etype == "eStorage":
            # create electric storage model
            model = eStorage(init_vals)

        # add model to model storage
        self.models.append(model)

        # add list for simulation data
        self.results.append([])

        # return model
        return model

    # perform simulation step of all models
    def step(self, time):
        """Perform a simulation step of all models

        Args:
            time (int): Current simulation time.
        """

        # Enumeration over all models in simulator
        for i, model in enumerate(self.models):
            # perform simulation step
            model.step(time)

            # collect data of model and storage local
            for j, signal in enumerate(model.results):
                self.results[i].append(getattr(model, signal))
