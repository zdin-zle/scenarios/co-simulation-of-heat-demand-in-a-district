"""
@author: Fernando Penaherrera @UOL/OFFIS

This class models a grid corrector that evaluates the grid 
if there is a troubling signal and produces a dictionary with
bus number and required power of the bateries.

Positive: Energy is required to be sent to the batteries (charging)
Negative: Energy needs to be removed from the battery (discharging)
"""
import copy
import pandapower as pp
from src.components.grid_corrector.correction_methods import vector_method, max_dependencies_method
import os
from src.common import GRID_SNAPS_DIR
from os.path import join


class GridCorrector:
    """A class to store the pandapower net and perform the calculations for required correction"""

    results = None  # Dict with the results.
    # This is not used here but called in the Super-Class Simulator.
    status = True  # True/False depending on grid condition. True if no violations.
    net = None  # Copy of the grid to correct.
    new_net = None  # Copy of the "corrected" grid.
    correction = None  # Dictionary with the correcions.
    no_cars = None  # Number of EVs. Used for data saving
    charging_strategy = None  # Name of chosen charging strategy. Used for data saving.

    def __init__(self, init_vals):
        """Initialization function

        Args:
            init_vals (dict): List of initial parameters to start the class.
        """
        # assign init values
        if init_vals is not None:
            for _, (key, value) in enumerate(init_vals.items()):
                setattr(self, key, value)

    def update_status(self, status):
        """The first thing is to update the grid status. This signal is received from the observer

        Args:
            status (boolean): Boolean representing the grid status. True = No violations. False = Violations
        """
        self.status = status

    def save_sto_power_before(self):
        """Saves the data on the power flows at the batteries BEFORE any correction is calculated.
        Used for comparison and to evaluate changes.
        """
        sto_p_before = {}

        # Pick the final status of the storage.
        for idx in self.net.storage.index:
            bus_id = self.net.storage.at[idx, "bus"]
            p_inj = self.net.storage.at[idx, "p_mw"]
            # create a dict with the power injection at the required buses
            sto_p_before[bus_id] = p_inj

        self.sto_p_before = sto_p_before

    def save_sto_power_after(self):
        """Saves the data on the power flows at the batteries AFTER correction is calculated.
        Used for comparison and to evaluate changes.
        """
        sto_p_after = {}

        # Pick the final status of the storage.
        for idx in self.new_net.storage.index:
            bus_id = self.new_net.storage.at[idx, "bus"]
            p_inj = self.new_net.storage.at[idx, "p_mw"]
            # create a dict with the power injection at the required buses
            sto_p_after[bus_id] = p_inj

        self.sto_p_after = sto_p_after

    def create_p_inj_dict(self):
        """Creates a dictionary with power flow changes for each battery."""
        bus_p_inj = {}  # Dictionary with the nodes and required power

        # Pick the difference between final and original status
        for idx in self.net.storage.index:
            bus_id = self.net.storage.at[idx, "bus"]
            bus_p_inj[bus_id] = {}
            bus_p_inj[bus_id]["p_inj"] = self.sto_p_after[bus_id] - self.sto_p_before[bus_id]
            bus_p_inj[bus_id]["vm_pu"] = self.net.res_bus.at[idx, "vm_pu"]
        self.bus_p_inj = bus_p_inj

    def step(self, time, status, net):
        """Advance the current model time

        Args:
            time (float): Size of the time step
            status (boolean): True if grid no issues.
            net (pandapower.net): An instanced copy of the net to be corrected
        """

        self.net = copy.deepcopy(net)  # Avoid problems with instancing.
        self.new_net = copy.deepcopy(net)  # Avoid problems with instancing.
        self.save_sto_power_before()

        # Prepare data cache
        self.correction = {}
        self.results = {}
        self.update_status(status)

        if self.status == False:  # false means a correction is required
            print("Correcting grid!!!")

            OUTPUT_DIR = os.path.join(GRID_SNAPS_DIR, str(self.no_cars), self.charging_strategy)

            if not os.path.exists(OUTPUT_DIR):
                os.makedirs(OUTPUT_DIR)
            # TODO: Fernando please integrate scenario, no of evs and chosen charging strategy in filename
            filename = join(OUTPUT_DIR, f"net_{time}.xlsx")
            pp.to_excel(self.net, filename)

            # TODO make the correction method as parameter?
            net_corr, _ = max_dependencies_method(net=self.net, iterationSteps=2000)
            # net_corr, _= vector_method(self.net, restricted=True, reactive=False)

            self.new_net = net_corr

            filename = join(OUTPUT_DIR, f"net_{time}_corr.xlsx")
            pp.to_excel(self.new_net, filename)

        # save the power flows after correction
        self.save_sto_power_after()

        # Create the dictionary in self.bus_p_inj with the required power in the batteries.
        self.create_p_inj_dict()


class Simulator(object):
    # constructor
    def __init__(self):
        # init data elments
        self.models = []
        self.results = []

    # create model instances
    def add_model(self, init_vals=None):
        # create grid_observer model
        model = GridCorrector(init_vals)

        # add model to the models list
        self.models.append(model)

        # add list for simulation data. This creates n list that will be slowly populated.
        self.results.append([])

        # return model
        return model

    # perform simulation step of all models
    def step(self, time, status, net):

        # Enumeration over all models in simulator
        for i, model in enumerate(self.models):
            # perform simulation step
            model.step(time, status, net)

            # collect data of model and storage local
            for _, signal in enumerate(model.results):
                self.results[i].append(getattr(model, signal))

    def add_controlled_grid(self, grid):
        self.grid = grid


if __name__ == "__main__":
    pass
