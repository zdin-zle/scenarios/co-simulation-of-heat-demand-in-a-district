"""
@author: Fernando Penaherrera @UOL/OFFIS

This is constructed to transform the list inputs of the buses into data models for the different components

It should divide the "dictionary" that is obtained from the "grid corrector" into different "data packages" to each bus

Since I do not know how to replicate the "children" from mosaik, this is my second best option.
Integration in the grid corrector simulator would be better, so as to avoid yet another instance. 

It provides the advantage of creating an Agent that can be set up for communication.
"""

import itertools
import mosaik_api
from src.components.grid_corrector import grid_node

# SIMULATION META DATA
META = {
    "type": "time-based",
    "models": {
        "GridNode": {
            "public": True,
            "params": ["model_params"],
            "attrs": [
                "list_input",  # the complete list with power correctiosn
                "p_inj",  # The separated power injection at the bus
                "vm_pu",  # The Voltage Level after correction for the said bus
            ],
        }
    },
    "extra_methods": [
        "add_controlled_storages",  # Adds the storages to the nodes
        "get_entities",  # Returns list with entities
    ],
}

# ------------INPUT-SIGNALS--------------------
# list_input           Complete list of the inputs calculated from the grid observer

# ------------OUTPUT-SIGNALS--------------------
# p_inj           Correction for this particular  bus.
# vm_pu           The Voltage Level after correction for the said bus


class Sim(mosaik_api.Simulator):
    """Simulator for the Nodes Agents

    Args:
        mosaik_api.Simulator (Class) : Inherited class for construction
    """

    def __init__(self):
        """Constructor"""
        super(Sim, self).__init__(META)

        # assign properties
        self.step_size = None

        # create dict for entities and eid_counters
        self.entities = {}
        self.eid_counters = {}

        # initialize simulator of model
        self.simulator = grid_node.Simulator()

    # init API
    def init(self, sid, time_resolution=1, start_date=None, step_size=1):
        """Initializer for the class

        Args:
            sid (class): id for the classes
            time_resolution (int, optional): Time resolution for the simulation. Defaults to 1.
            start_date (int, optional): Start Date. Defaults to None.
            step_size (int, optional): Step size of the simulation. Defaults to 1.

        Returns:
            dict: Metadata of the simulation
        """
        # assign properties
        self.sid = sid
        self.time_resolution = time_resolution
        self.step_size = step_size

        # return meta data
        return self.meta

    def add_controlled_storages(self, storages):
        """Adds a list of storages to monitor

        Args:
            storages (storage.eStorage): Model of a storage.
        """
        self.simulator.add_controlled_storages(storages)

    # create model
    def create(self, num, model_type, pv_data=None):
        """Creates instances of the Node model agent

        Args:
            num (int): Number of Nodes to model
            model_type (class): Description of the class
            pv_data (dict, optional): Dictionary with data of the pv inputs. Defaults to None.

        Returns:
            _type_: _description_
        """
        if pv_data:
            for attr, val in pv_data.items():
                setattr(self, attr, val)

        counter = self.eid_counters.setdefault(model_type, itertools.count())

        # create list for entities
        entities = []

        # TODO replace string formating
        for i in range(num):
            eid = "{}_{}".format(model_type, next(counter))  # Entities IDs

            # create new Grid Node Agent model
            model = self.simulator.add_model({"name": i})

            # create full id
            full_id = self.sid + "." + eid

            self.entities[eid] = {
                "ename": eid,
                "etype": model_type,
                "model": model,
                "full_id": full_id,
            }
            entities.append({"eid": eid, "type": model_type, "rel": []})

        return entities

    # perform calculation step
    def step(self, time, inputs, max_advance=360000):
        """Performs a step of the simulation models

        Args:
            time (int): Time step size. Given by MOSAIK
            inputs (dict): Inputs for simulation models. Given by MOSAIK
            max_advance (int, optional): Max Advance. Defaults to 360000.

        Returns:
            int: New time stamp in seconds
        """
        self.cache = {}

        for eid, attrs in inputs.items():
            for attr, vals in attrs.items():
                if attr == "list_input":
                    list_input = list(vals.values())[0]

        # Perform simulation step
        self.simulator.step(time, list_input)

        # self.cache[eid] = self.simulator.values
        # Next timestamp for simulation
        return time + self.step_size

    # get data
    def get_data(self, outputs):
        """Get data from the simulation

        Args:
            outputs (dict): Dict with expected outputs.

        Raises:
            ValueError: Error if attribute not in metadata

        Returns:
            dict: Dictionary with data to be read by MOSAIK
        """
        # create output data
        data = {}

        # loop over all output values
        for ename, attrs in outputs.items():

            # set data entry for model
            data[ename] = {}

            # loop over all attributes
            for attr in attrs:
                if attr not in self.meta["models"]["GridNode"]["attrs"]:
                    raise ValueError("Unknown output attribute: %s" % attr)

                # get model data
                data[ename][attr] = getattr(self.entities[ename]["model"], attr)
        return data

    def get_entities(self):
        """Returns entities of API

        Returns:
            list: List with the entities and their properties
        """
        return self.entities


if __name__ == "__main__":
    pass
