"""
Mosaik interface for the electrical load of GHD.

@author: Christian Reinhold and Henrik Wagner elenia@TUBS

"""


import os, arrow, pandas as pd
from os.path import join
from src.common import DATA_PV_DIR, DATA_LOAD_DIR


class household:
    """CSV-Data-Read for household profiles"""

    # static properties
    type = "household"  # model type

    # electric properties
    cos_phi = 1  # cos phi [-]

    # static properties - file
    datafile = None  # relative path of datafile
    date_format = "YYYY-MM-DD HH:mm:ss"  # format of time vector
    date_format_dt = "%Y-%m-%d %H:%M:%S"  # format of time vectore for datetime
    delimiter = ","  # delimiter
    header_rows = 2  # header rows
    start_date = "2014-01-01 00:00:00"  # start time in time vector
    time_resolution = 1  # time resolution per step

    # local properties
    data = None  # data of file [-]
    row_data = None  # row data [-]

    # dynamic input properties
    SIGNAL_PROGNOSIS = 0  # Signal to create new prognosis of load
    PROGNOSIS_START = None  # Starting time of forecast horizon
    PROGNOSIS_END = None  # End time of forecast horizon

    # dynamic output properties
    P = None  # Active Power [W]
    Q = None  # Reactive Power [var]
    PROGNOSIS_LOAD = 0  # Load prognosis for forecast horizon

    # time for hdf5
    timestamp = None

    # result properties
    results = ["P", "Q"]

    # constructor
    def __init__(self, init_vals):
        """Initializes the mosaik-load model

        Args:
            init_vals (dict): Initial values for creation of a houshold load model. Read from a CSV file
        """

        # assign init values
        for i, (key, value) in enumerate(init_vals.items()):
            setattr(self, key, value)

        # convert time formats
        self.start_date = arrow.get(self.start_date, self.date_format)

        # open data file
        status = self.open()

        # check data format file
        self.check_data()

        # set init timestamp
        self.timestamp = self.start_date

    # open file
    def open(self):
        """Opens a CSV file

        Returns:
            status (bool): Returns whether the file was opened successfully
        """

        # set status
        status = False

        # check if file exist
        if os.path.exists(self.datafile):
            # load io datastream
            self.data = open(self.datafile)

            # load csv as pandas dataframe
            self.data_pd = pd.read_csv(self.datafile, header=(list(range(self.header_rows))))

            # set status
            status = True

        # return status
        return status

    # check data of file
    def check_data(self):
        """Checks if row contains the simulation start date

        Raises:
            ValueError: Start date is not in CSV file
        """

        # jump over head lines
        for i in range(self.header_rows):
            self._read_next_row()

        # Check start date
        self._read_next_row()
        if self.start_date < self.row_data[0]:
            raise ValueError(
                'Start date "%s" not in CSV file.' % self.start_date.format(self.date_format)
            )
        while self.start_date > self.row_data[0]:
            self._read_next_row()
            if self.row_data is None:
                raise ValueError(
                    'Start date "%s" not in CSV file.' % self.start_date.format(self.date_format)
                )

        # TODO: Implement check for pandas data

    # read next data row
    def _read_next_row(self):
        """Reads the next row of the CSV file into an array"""

        try:
            self.row_data = next(self.data).strip().split(self.delimiter)
            self.row_data[0] = arrow.get(self.row_data[0], self.date_format)
        except StopIteration:
            self.row_data = None
        except:
            self.row_data = None

    # perform simulation step of household load
    def step(self, time):
        """Gets the active power from the next row of the CSV file

        Args:
            time (int): Current simulation time
        Raises:
            IndexError: End of CSV file reached
        """

        # check if row_data empty
        if self.row_data is None:
            raise IndexError("End of CSV file reached.")

        # calculate expected date
        expected_date = self.start_date.shift(seconds=time * self.time_resolution)

        # check until right row
        while self.row_data[0] < expected_date:
            # Next time stamp
            self._read_next_row()

        # assign active power
        self.P = float(self.row_data[1])
        self.Q = (1 - self.cos_phi) * self.P

        # check if load prognosis is requested
        if (
            self.SIGNAL_PROGNOSIS == 1
            and self.PROGNOSIS_START != None
            and self.PROGNOSIS_END != None
        ):
            # convert input prognosis timespan to arrow datetime
            prognosis_start_date = (self.start_date.shift(seconds=self.PROGNOSIS_START)).format(
                self.date_format
            )
            prognosis_end_date = (self.start_date.shift(seconds=self.PROGNOSIS_END)).format(
                self.date_format
            )

            # find indices for prognosis timespan in load data
            row = self.data_pd[self.data_pd["Time"] == prognosis_start_date].iloc[0]
            index_start = row.name
            row = self.data_pd[self.data_pd["Time"] == prognosis_end_date].iloc[0]
            index_end = row.name

            # sumarize energy over prognosed timespan
            prognosis_timespan = self.data_pd.iloc[index_start : index_end + 1]
            # TODO: Implement flexible time steps
            self.PROGNOSIS_LOAD = prognosis_timespan[self.data_pd.columns[1]].sum() * 0.25

        # Next time stamp
        self._read_next_row()


class PV:
    """CSV-Data-Reader for PV profiles"""

    # static properties
    type = "PV"  # model type

    # electric properties
    cos_phi = 1  # cos phi [-]

    # static properties - file
    datafile = None  # relative path of datafile
    date_format = "YYYY-MM-DD HH:mm:ss"  # format of time vector
    date_format_dt = "%Y-%m-%d %H:%M:%S"  # format of time vectore for datetime
    delimiter = ","  # delimiter
    header_rows = 1  # header rows
    start_date = "2020-01-01 00:00:00"  # start time in time vector
    time_resolution = 1  # time resolution per step

    # local properties
    data = None  # data of file [-]
    row_data = None  # row data [-]

    # dynamic input properties
    SIGNAL_PROGNOSIS = None  # Signal to create new prognosis of load
    PROGNOSIS_START = None  # Starting time of forecast horizon
    PROGNOSIS_END = None  # End time of forecast horizon

    # dynamic output properties
    P = None  # Active Power [W]
    Q = None  # Reactive Power [var]
    PROGNOSIS_PV = None  # PV prognosis for forecast horizon

    # time for hdf5
    timestamp = None

    # result properties
    results = ["P", "Q"]

    # constructor
    def __init__(self, init_vals):
        """Initializes the mosaik-load model

        Args:
            init_vals (dict): Initial values for creation of a houshold load model. Read from a CSV file
        """

        # assign init values
        for i, (key, value) in enumerate(init_vals.items()):
            setattr(self, key, value)

        # convert time formats
        self.start_date = arrow.get(self.start_date, self.date_format)

        # open data file
        self.open()

        # check data format file
        self.check_data()

        # set init timestamp
        self.timestamp = self.start_date

    # open file
    def open(self):
        """Opens a CSV file"""

        # get absolute path for emobpy pickle
        path_pv_absolute = (self.datafile.split("pv")[-1])[1:]
        path_pv_absolute = join(DATA_PV_DIR, path_pv_absolute)

        # check if file exist
        if os.path.exists(self.datafile):
            # load io datastream
            self.data = open(self.datafile)

            # load csv as pandas dataframe
            self.data_pd = pd.read_csv(self.datafile, header=(list(range(self.header_rows))))

        elif os.path.exists(path_pv_absolute):
            self.data = open(path_pv_absolute)

            # load csv as pandas dataframe
            self.data_pd = pd.read_csv(path_pv_absolute, header=(list(range(self.header_rows))))

        else:
            raise ImportError("pv .csv-files could not beloaded! Check path!")

    # check data of file
    def check_data(self):
        """Checks if row contains the simulation start date

        Raises:
            ValueError: Start date is not in CSV file
        """

        # jump over head lines
        for i in range(self.header_rows):
            self._read_next_row()

        # Check start date
        self._read_next_row()
        if self.start_date < self.row_data[0]:
            raise ValueError(
                'Start date "%s" not in CSV file.' % self.start_date.format(self.date_format)
            )
        while self.start_date > self.row_data[0]:
            self._read_next_row()
            if self.row_data is None:
                raise ValueError(
                    'Start date "%s" not in CSV file.' % self.start_date.format(self.date_format)
                )

    # read next data row
    def _read_next_row(self):
        """Reads the next row of the CSV file into an array"""

        try:
            self.row_data = next(self.data).strip().split(self.delimiter)
            self.row_data[0] = arrow.get(self.row_data[0], self.date_format)
        except StopIteration:
            self.row_data = None
        except:
            self.row_data = None

    # perform simulation step of household load
    def step(self, time):
        """Gets the active power from the next row of the CSV file

        Args:
            time (int): Current simulation time
        Raises:
            IndexError: End of CSV file reached
        """

        # check if row_data empty
        if self.row_data is None:
            raise IndexError("End of CSV file reached.")

        # calculate expected date
        expected_date = self.start_date.shift(seconds=time * self.time_resolution)

        # check until right row
        while self.row_data[0] < expected_date:
            # Next time stamp
            self._read_next_row()

        # assign active power
        self.P = float(self.row_data[1])
        self.Q = (1 - self.cos_phi) * self.P

        # check if load prognosis is requested
        if (
            self.SIGNAL_PROGNOSIS == 1
            and self.PROGNOSIS_START != None
            and self.PROGNOSIS_END != None
        ):
            # convert input prognosis timespan to arrow datetime
            prognosis_start_date = (self.start_date.shift(seconds=self.PROGNOSIS_START)).format(
                self.date_format
            )
            prognosis_end_date = (self.start_date.shift(seconds=self.PROGNOSIS_END)).format(
                self.date_format
            )

            # find indices for prognosis timespan in load data
            row = self.data_pd[self.data_pd["Time"] == prognosis_start_date].iloc[0]
            index_start = row.name
            row = self.data_pd[self.data_pd["Time"] == prognosis_end_date].iloc[0]
            index_end = row.name

            # sumarize energy over prognosed timespan
            prognosis_timespan = self.data_pd.iloc[index_start : index_end + 1]
            # TODO: Implement flexible time steps
            self.PROGNOSIS_PV = prognosis_timespan[self.data_pd.columns[1]].sum() * 0.25

        # Next time stamp
        self._read_next_row()


# Simulates a number of ``Model`` models and collects some data.
class Simulator(object):
    """Simulator class for mosaik-load model
    Creates multiple instances and saves the results in an accesible list

    Args:
        object (class): Generic class
    """

    # contructor
    def __init__(self):
        """Constructor"""

        # init data elments
        self.models = []
        self.results = []

    # create model instances
    def add_model(self, type="household", init_vals=None):
        """Create model instances of mosaik-load

        Args:
            type (str): name of load model (default to 'household)
            init_vals (dict): List (length=num) with initial values for each load model

        Returns:
            model: returns created load object
        """

        # select by type household or PV
        if type == "household":
            # create household model
            model = household(init_vals)

        elif type == "PV":
            # create household model
            model = PV(init_vals)

        else:
            raise Exception("Unknown type selected for load model.")

        # add model to model storage
        self.models.append(model)

        # add list for simulation data
        self.results.append([])

        # return model
        return model

    # perform simulation step of all models
    def step(self, time):
        """Performs simulation step of all to simulator connected models

        Args:
            time (int): Current simulation time.
        """

        # Enumaration over all models in simulator
        for i, model in enumerate(self.models):
            # perform simulation step
            model.step(time)

            # collect data of model and storage local
            for j, signal in enumerate(model.results):
                self.results[i].append(getattr(model, signal))
