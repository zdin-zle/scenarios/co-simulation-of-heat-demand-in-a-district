"""
@author: Christian Reinhold and Henrik Wagner elenia@TUBS

The mosaik-control was developed as
an energy management system on the building level and is
therefore connected to all of the building's components. The
control gathers all of the component's power flows, calculates
the energy balance at the building node and the component's
flexibility, and then sends out power set values in each step.
Hereby, available solar power is favored to grid consumption,
as well as charging/discharging limits of the charging stations,
and batteries are considered.

"""


class BEM:
    """Models an energy managament system on building level"""

    # static properties
    type = "BEM"  # model type
    components = []  # controlled components by control unit [-]

    # added for self identification (FPV)
    name = ""
    model_number = None
    soc_min = None  # minimun SOC for the Grid Observer Scenario
    soc_max = None  # minimun SOC for the Grid Observer Scenario

    # dynamic input properties
    P_LOAD = None  # Active Power Load [W]
    Q_LOAD = None  # Reactive Power Load [var]
    P_PV = None  # Active Power PV [W]
    P_MIN = None  # Maximum active power [W]
    P_MAX = None  # Maximum reactive power [W]
    SIGNAL_PROGNOSIS_CS_control = (
        None  # Signal to request new forecast period based on BEV_consumption_period [-]
    )
    BEV_consumption_period = None  # Electricity consumption of EV in next period not being home,
    # starting right after STANDING_TIME_END
    PROGNOSIS_START = None  # Starting time of forecast horizon
    PROGNOSIS_END = None  # End time of forecast horizon
    PROGNOSIS_LOAD = None  # Load prognosis for forecast horizon
    PROGNOSIS_PV = None  # PV prognosis for forecast horizon

    # dynamic output properties
    P_SET = {}  # Set-Point for components [W]
    P_SALDO = None  # Saldo active power [W]
    P_SALDO_RES = {}  # Residual renewable power [W]
    SIGNAL_PROGNOSIS = None  # Output signal to request prognosis from load/pv
    PROGNOSIS_RESIDUAL = {}  # Residual prognosis calculated by load & pv prognoosis [Wh]

    # helper signal to check if surplus energy is available
    RES_SIGNAL = {}

    # result properties
    results = ["P_SET", "P_SALDO"]

    # set data identifier
    set_data = [
        "P_MIN",
        "SIGNAL_PROGNOSIS",
        "PROGNOSIS_START",
        "PROGNOSIS_END",
        "PROGNOSIS_RESIDUAL",
    ]

    # init of values
    def __init__(self, init_vals):
        """Initializes the mosaik-cs model

        Args:
            init_vals (dict, optional): Initial values for creation of BEM model. Normally not used.
        """

        # assign init values
        if init_vals:
            for _, (key, value) in enumerate(init_vals.items()):
                setattr(self, key, value)

    # perform simulation step
    def step(self, time):
        """Gathers current energy flows, estimates energy balances and
        then calculates power set values for each connected component.

        Args:
            time (int): Current simulation time
        """

        # SIGNAL_PROGNOSIS
        if self.PROGNOSIS_LOAD != None:
            pass
            # print("Kommunikation CONTROL erfolgreich")

        # set active power saldo
        self.P_SALDO = sum(self.P_LOAD.values())

        # HOUSEHOLDS: manage signal exchange between household and charging stations
        for i, _ in self.eid["household"].items():
            # loop over connected CS
            for j, _ in self.eid["charging_station"].items():
                # NOTE: currently only works with one connected CS station
                # set signal for prognosis in connected loads
                self.SIGNAL_PROGNOSIS[i] = self.SIGNAL_PROGNOSIS_CS_control[j]
                self.PROGNOSIS_START[i] = self.PROGNOSIS_START[j]
                self.PROGNOSIS_END[i] = self.PROGNOSIS_END[j]

        # PV SYSTEM
        # NOTE: Does not work for scenarios using "PVSim", STATUS == "run"/"test"
        for i, _ in self.eid["PV"].items():

            # assign actual uncontrolled active power
            self.P_SALDO += -self.P_PV[i]  # changed symbol
            # set control value
            self.P_SET[i] = -self.P_PV[i]

            # set signal for prognosis (from CS) in connected PVs
            for j, _ in self.eid["charging_station"].items():
                self.SIGNAL_PROGNOSIS[i] = self.SIGNAL_PROGNOSIS_CS_control[j]
                self.PROGNOSIS_START[i] = self.PROGNOSIS_START[j]
                self.PROGNOSIS_END[i] = self.PROGNOSIS_END[j]

        # CHARGING STATION
        for i, _ in self.eid["charging_station"].items():

            # conditional statements to check if keys (i) are present in P_MAX
            if i in self.P_MAX.keys():

                # set set point
                self.P_SET[i] = self.P_MAX[i]

                # assign actual uncontrolled active power
                self.P_SALDO += self.P_SET[i]

                # check for renewable power surplus
                if self.P_SALDO >= 0:
                    self.RES_SIGNAL[i] = False
                    self.P_SALDO_RES[i] = 0

                elif self.P_SALDO < 0:
                    self.RES_SIGNAL[i] = True
                    # save renewable power surplus
                    self.P_SALDO_RES[i] = self.P_SALDO

                # calculating the prognosis for residual load without considering battery storage
                for household, load_prognosis in self.PROGNOSIS_LOAD.items():
                    for pv, pv_prognosis in self.PROGNOSIS_PV.items():
                        if load_prognosis and pv_prognosis is not None:
                            self.PROGNOSIS_RESIDUAL[i] = float(load_prognosis - pv_prognosis)

                        else:
                            self.PROGNOSIS_RESIDUAL[i] = float(0)

        # BATTERY STORAGES
        for i, storage in self.eid["eStorage"].items():

            # conditional statement to check if keys are in storages
            if i in self.P_MIN.keys() and i in self.P_MAX.keys():

                # check if too much load
                if self.P_SALDO > 0:
                    val = min(self.P_SALDO, self.P_MIN[i])

                    # This part only if there is grid_observer
                    if self.soc_min:

                        # print(f"controling charge in bus {self.model_number}", end= "\r")
                        sto_model = storage["model"]
                        soc = sto_model.SOC
                        soc_lim = self.soc_min  # 0.1 for example
                        E_BAT = sto_model.E_BAT
                        E_bat_rated = sto_model.E_bat_rated

                        if soc <= soc_lim:
                            val = 0

                        if (E_BAT - val * 15 * 60 / 3600) / E_bat_rated <= soc_lim:
                            val = 0

                    self.P_SET[i] = -val  # -min(self.P_SALDO, self.P_MIN[i])

                elif self.P_SALDO < 0:
                    val = min(abs(self.P_SALDO), self.P_MAX[i])
                    if self.soc_max:
                        sto_model = storage["model"]
                        soc = sto_model.SOC

                        soc_lim_max = self.soc_max  # 0.9 for example
                        E_BAT = sto_model.E_BAT
                        E_bat_rated = sto_model.E_bat_rated

                        if soc >= soc_lim_max:
                            val = 0

                        if (E_BAT + val * 15 * 60 / 3600) / E_bat_rated > soc_lim_max:
                            # print(f"controling charge in bus ", self.name, ":", soc)# , end= "\r")
                            val = 0
                        # assert(val <= sto_model.P_CHARGE_MAX)
                    self.P_SET[i] = val
                elif self.P_SALDO == 0:
                    self.P_SET[i] = 0

                # calculate final power bilance
                self.P_SALDO += self.P_SET[i]

            else:
                pass

    # update internal structure for components
    def update_controlled_entity(self):
        """Updates the connected/controlled components of the BEM"""
        # clear storage components
        self.eid = {"PV": {}, "eStorage": {}, "charging_station": {}, "household": {}}

        # get all component types
        for component in self.components:
            for _, values in component.items():
                if values["etype"] not in self.eid:
                    self.eid[values["etype"]] = {}
                self.eid[values["etype"]][values["full_id"]] = values

    # get component by type
    def get_component_type(self, typpe):
        """_summary_

        Args:
            typpe (any): Input to determine all components of this type.

        Returns:
            dict: Returns all component of input "typpe"
        """

        # init data element
        data = {}

        # get all component types
        for component in self.components:
            for attr, values in component.items():
                if values["etype"] == typpe:
                    data[attr] = values

        return data


class Simulator(object):
    """Simulator class for mosaik-control model"""

    def __init__(self):
        """Constructor"""
        # initialize data elements
        self.models = {}

    # create model instance
    def add_model(self, model_type, model_name, init_vals):
        """Creates instance of control model, called from control_simulator

        Args:
            model_type (str): Describing the model type (here BEM)
            model_name (str): Name of model consists of type and number (e.g. BEM_0)
            init_vals (dict, optional): List (length=num) with initial values for each BEM model.
            Optional for BEM model

        Returns:
            model: returns created BEM object
        """

        # create model with init values
        if model_type == "BEM":
            model = BEM(init_vals)

        # add model to model storage
        self.models[model_name] = {
            "name": model_name,
            "type": model_type,
            "model": model,
            "results": [],
        }

        # return model class
        return model

    # add controlled unit
    def add_controlled_entity(self, models, components):
        """Adds a controlled component to the respective BEM

        Args:
            models (dict): Input list with all models (model_names) of type BEM, which need to
            be filtered for correct BEM.
            components (dict): Input list with all models of a type (e.g. charging_station) which
            will be connected to BEM.
        """

        # append model to list
        keys_model = list(models.keys())
        keys_comp = list(components.keys())
        for key_model, key_comp in zip(keys_model, keys_comp):
            model = models[key_model]["model"]
            model.name = models[key_model]["ename"]
            model.number = int(model.name.split("_")[-1])
            # comp = components[key_comp]
            model.components.append({key_comp: components[key_comp]})

            components_list = []
            # remove all "components" with different numbers than corresponding BEM
            for comp in model.components:
                id_num = int(list(comp)[0].split("_")[-1])
                # NOTE: Currently fixed settings: range(10) and number(49) set according to Am Ölper Berge district
                for i in range(10):
                    if id_num == model.number + 49 * i:
                        components_list.append(comp)

            # assign new list to our model object...
            model.components = components_list
            model.update_controlled_entity()

    # perform simulation step
    def step(self, time):
        """Performs simulation step of all to simulator connected models

        Args:
            time (int): Current simulation time.
        """

        # Enumeration over all models in simulator
        for i, model_entry in self.models.items():
            # perform simulation step
            model_entry["model"].step(time)

            # collect data of model and storage local
            for j, signal in enumerate(model_entry["model"].results):
                model_entry["results"].append(getattr(model_entry["model"], signal))

    # OUTPUT: get data for set data
    def get_set_data(self, inputs):
        """Gets

        Args:
            inputs (dict): All mosaik-control models and their exchanged values (according to META)

        Returns:
            dict: Returns dict (data) with all set values for all BEM,
            e.g. {BEM_0:{CSSim-0.charging_station_0: {P_SET : 0, RES_Signal: False}}}
        """

        # create data element
        data = {}

        # loop over all models
        for eid, attrs in inputs.items():

            # get model entry
            model_entry = self.models[eid]

            # create empty dict for signal partner
            signal_partners = {}

            # get signal partner
            # for data_identifier in ["P_MIN", 'P_SALDO']:
            #     signal_partners.update(attrs.get(model_entry["model"].data_identifier, {}))

            for i in range(len(model_entry["model"].set_data)):
                signal_partners.update(attrs.get(model_entry["model"].set_data[i], {}))

            # loop over all signal partners
            for model_eid, _ in signal_partners.items():

                # create entry
                if eid not in data:
                    data[eid] = {}
                if eid not in data[eid]:
                    data[eid][model_eid] = {}

                # get signals for household prognosis
                if model_eid[0:4] == "Load" or model_eid[0:2] == "PV":
                    data[eid][model_eid]["SIGNAL_PROGNOSIS"] = model_entry[
                        "model"
                    ].SIGNAL_PROGNOSIS[model_eid]
                    data[eid][model_eid]["PROGNOSIS_START"] = model_entry["model"].PROGNOSIS_START[
                        model_eid
                    ]
                    data[eid][model_eid]["PROGNOSIS_END"] = model_entry["model"].PROGNOSIS_END[
                        model_eid
                    ]

                # get P_SET values from models
                else:
                    data[eid][model_eid]["P_SET"] = model_entry["model"].P_SET[model_eid]

                # prepare signal exchange from/to charging station
                if model_eid[0:5] == "CSSim":
                    data[eid][model_eid]["RES_SIGNAL"] = model_entry["model"].RES_SIGNAL[model_eid]
                    data[eid][model_eid]["P_SALDO_RES"] = model_entry["model"].P_SALDO_RES[
                        model_eid
                    ]  # have to change for the more smarter solar charger
                    data[eid][model_eid]["SIGNAL_PROGNOSIS_CS_control"] = model_entry[
                        "model"
                    ].SIGNAL_PROGNOSIS_CS_control[model_eid]
                    data[eid][model_eid]["PROGNOSIS_RESIDUAL"] = model_entry[
                        "model"
                    ].PROGNOSIS_RESIDUAL[model_eid]

        # return data
        return data
