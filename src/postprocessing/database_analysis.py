'''
@author: Fernando Penaherrera @UOL/OFFIS

#### This is useful for development for quick analysis. I would prefer NOT 
#### to delete this and keep it as reference

This file contains functions for Quick analysis of the resulting database
for a rapid evaluation. This is useful for development
'''
import logging
from os.path import join
import h5py
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from src.common import Scenarios, RESULTS_POSTPROC_DIR, RESULTS_DB_DIR
import os
import seaborn as sns

sns.set_theme()
sns.set_style("white")

logging.basicConfig(level=logging.INFO)


def analyze_database(scenario=Scenarios.TEST, configuration=None):
    """Reads the data stored in the HDF5 database,
    converts to a Data Frame and plot the pv_results
    
    Args:
        scenario (Scenarios.enum, optional): One the Scenarios in common.py. Defaults to Scenarios.TEST.
        
        configuration (dict, optional): Simulation configuration dictionary. Defaults to None.
            configuration = {
                "start": '2020-03-02 00:00:00',
                "end":  24* 3600 * 31,
                "step_size": 60 * 15,
                "overwrite_data": False,
                "n_cars": 49,
                "status": "test"} 
    """    

    n_cars = configuration["n_cars"]
    START = configuration["start"]
    periods = int(configuration["end"]/configuration["step_size"])
    freq_min = int(configuration["step_size"]/60)
    dti = pd.date_range(start=START, periods=periods, freq=f"{freq_min}T")

    #TODO The database name shall have also the charging strategy
    DATABASE_FILE = join(
        RESULTS_DB_DIR, f"Results_{scenario.name}_{n_cars}.hdf5")

    N_CARS = "{:02d}".format(n_cars)
    data = h5py.File(DATABASE_FILE, "r")
    OUTPUT_DIR = join(RESULTS_POSTPROC_DIR, scenario.name, str(N_CARS))

    if not os.path.exists(OUTPUT_DIR):
        os.makedirs(OUTPUT_DIR)

    series = {}
    # Which datarows do I want
    series["bus"] = [b for b in data["Series"].keys() if "bus" in b.lower()]
    series["pv"] = [b for b in data["Series"].keys() if "pvsim" in b.lower()]
    series["load"] = [b for b in data["Series"].keys() if "load" in b.lower()]
    series["storage"] = [b for b in data["Series"].keys() if "eStorage" in b]
    series["trafo"] = [b for b in data["Series"].keys() if "None" in b]
    series["BEM"] = [b for b in data["Series"].keys() if "BEM" in b]
    series["CarSim"] = [b for b in data["Series"].keys() if "CarSim" in b]
    series["storage_grid"] = [b for b in data["Series"].keys() if "sto_" in b]
    series["ChargStation"] = [
        b for b in data["Series"].keys() if "charging_station" in b]
    series["gridObs"] = [b for b in data["Series"].keys() if "GridObserver" in b]
    series["GridNode"] = [b for b in data["Series"].keys() if "GridNode" in b]

    # Which parameters do I want?
    params = {}
    params["bus"] = ["p_mw", "vm_pu"]  # , "q_mvar",
    params["pv"] = ["P"]  # , "Q"]
    params["load"] = ["p_mw"]
    params["storage"] = ["P_SET", 'SOC', 'P',
                         'P_CHARGE_MAX', 'P_DISCHARGE_MAX']
    params["trafo"] = ["loading_percent"]  # , "tap_pos"]
    params["BEM"] = ["P_SALDO"]
    params["CarSim"] = ["SOC"]
    params["ChargStation"] = ["P"]
    params["storage_grid"] = ["p_mw", "max_p_mw", "min_p_mw"]
    params["gridObs"] = ["status"]
    params["GridNode"] = ["p_inj", "vm_pu"]

    # only the first 3 items for analysis
    for element, column_names in series.items():
        if True:  # element in ["storage", "bus", "trafo", "gridObs"]:
            params_list = params[element]
            for par in params_list:
                # print(par)
                if par == "status":
                    try:
                        output_list = []
                        results = {}
                        for column in column_names:
                            output = data["Series"][column][par]
                            output = np.array(output)
                            results[column.split("-")[-1]] = np.array(output)*1
                        df = pd.DataFrame(results)
                        fig, ax1 = plt.subplots(figsize=(8, 6))
                        sns.lineplot(data=df, ax=ax1)
                        ax1.set_title(
                            f"Co-Simulation Results \n {scenario.name}-{element}-{par}")
                        ax1.set_ylabel('Grid Observer Status')
                        ax1.set_ylim([0, 1.1])
                        plt.savefig(os.path.join(
                            OUTPUT_DIR,
                            f"{scenario.name}_{element}_{par}.jpg"))

                        fig.close()

                    except:
                        pass
                try:
                    output_list = []
                    results = {}
                    max_val, min_val = 0, 0
                    column_with_max = column_names[0]
                    column_with_min = column_names[0]
                    # search for extremes:
                    for column in column_names:
                        output = data["Series"][column][par]
                        output = np.array(output)
                        maxi = max(output)
                        mini = min(output)
                        if maxi > max_val:
                            max_val = maxi
                            column_with_max = column

                        if mini < min_val:
                            min_val = mini
                            column_with_min = column

                    extremes = [column_with_max, column_with_min]

                    for column in [*column_names[0:4], *extremes]:
                        output = data["Series"][column][par]
                        output_list.append(output)

                        results[column.split("-")[-1]] = np.array(output)

                    df = pd.DataFrame(results)
                    df["Time"] = dti[0:df.shape[0]]
                    df.set_index("Time", inplace=True)

                    fig, ax1 = plt.subplots(figsize=(8, 6))

                    sns.lineplot(data=df, ax=ax1)
                    ax1.set_title(
                        f"Co-Simulation Results \n {scenario.name}: {element} - {par}")
                    if par in ["p_mw", "P_SET", "P"]:
                        ax1.set_ylabel('Active Power Flow, W')
                    if par == "q_mvar":
                        ax1.set_ylabel('Reactive Power Flow, kVAr')
                    if par == "loading_percent":
                        ax1.set_ylabel('Loading Percent')
                    if par == "SOC":
                        ax1.set_ylabel('State of Charge')
                    fig.savefig(os.path.join(
                        OUTPUT_DIR,
                        f"{scenario.name}_{element}_{par}.jpg"))
                    fig.close()
                except Exception:
                    pass

    logging.info("Database results plotted in \n {}".format(
        OUTPUT_DIR))

    plt.close("all")


if __name__ == '__main__':
    configuration = {
        "start": '2020-03-02 00:00:00',
        "end":  24 * 3600 * 15,
        "step_size": 60 * 15,
        "overwrite_data": False,
        "n_cars": 49*5,
        "status": "test"}  # "test", "run"

    analyze_database(scenario=Scenarios.EV_PV_STO, configuration=configuration)
