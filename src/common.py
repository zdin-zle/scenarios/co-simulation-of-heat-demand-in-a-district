import os
from os.path import join
from enum import Enum

def get_project_root():
    """
    Returns the path to the project root directory.

    Returns:
        str: A string with the project root directory
    """
    return os.path.realpath(os.path.join(os.path.dirname(__file__), os.pardir))

# Base directories
BASE_DIR = get_project_root()

# Data directories
DATA_DIR = join(BASE_DIR, "data")
DATA_PROCESSED_DIR = join(DATA_DIR, "processed")
DATA_PROCESSED_LOAD_DIR = join(DATA_PROCESSED_DIR, "load")
DATA_PROCESSED_PV_DIR = join(DATA_PROCESSED_DIR, "pv")
DATA_RAW_DIR = join(DATA_DIR, "raw")

# Documentation
DOCS_DIR = join(BASE_DIR, "docs")

# Evaluation
EVALUATION_DIR = join(BASE_DIR, "evaluation")
EVALUATION_FIGS_DIR = join(EVALUATION_DIR, "figs")
EVALUATION_VIS_DIR = join(EVALUATION_DIR, "visualization")

# Results
RESULTS_DIR = join(BASE_DIR, "results")
RESULTS_DB_DIR = join(RESULTS_DIR, "databases")
RESULTS_GRID_SNAPS_DIR = join(RESULTS_DIR, "grid_snapshots")
RESULTS_REPORTS_DIR = join(RESULTS_DIR, "reports")
RESULTS_POSTPROC_DIR = join(RESULTS_DIR, "postprocessed")

# SRC
SRC_DIR = join(BASE_DIR, "src")
SRC_COMPONENTS_DIR = join(SRC_DIR, "components")
SRC_POSTPROC_DIR = join(SRC_DIR, "postprocessing")
SRC_PREPROC_DIR = join(SRC_DIR, "data_processing")
SRC_SCENARIOS_DIR = join(SRC_DIR, "scenarios")

# Test
TEST_DIR = join(BASE_DIR, "tests")


folder_list_to_test = [DATA_DIR, DATA_PROCESSED_DIR, DATA_PROCESSED_LOAD_DIR, DATA_PROCESSED_PV_DIR, DATA_RAW_DIR,
                           DOCS_DIR, EVALUATION_DIR, EVALUATION_FIGS_DIR, EVALUATION_VIS_DIR, RESULTS_DIR, RESULTS_DB_DIR,
                           RESULTS_GRID_SNAPS_DIR, RESULTS_REPORTS_DIR, RESULTS_POSTPROC_DIR, SRC_DIR, SRC_COMPONENTS_DIR,
                           SRC_POSTPROC_DIR, SRC_PREPROC_DIR, SRC_SCENARIOS_DIR, TEST_DIR]
    

def check_and_create_folders(folder_list):
    """
    Check if the folders in the list exist, if not, create them.

    Args:
        folder_list (list): List of folder paths to check and create if necessary
    """
    for folder in folder_list:
        if not os.path.exists(folder):
            os.makedirs(folder)
            print(f"Folder '{folder}' created successfully.")
        else:
            print(f"Folder '{folder}' already exists.")

class Scenarios(Enum):
    """
    List of the available SIMULATION SCENARIOS
    """
    TEST = 0
    EV = 1
    EV_PV = 2
    EV_PV_STO = 3
    GRID_OBSV = 4

class Heating(Enum):
    """
    Heating Scenarios 
    """
    HEAT_PUMP = 0
    GAS = 1

class Charging_Strategies(Enum):
    """
    List of available charging strategies for EV
    """
    max_P = 0
    forecast = 1
    night_charging = 2
    solar_charging = 3
    random = 4

if __name__ == "__main__":
    # List of folders to test the function
    
    # Test the function
    check_and_create_folders(folder_list_to_test)