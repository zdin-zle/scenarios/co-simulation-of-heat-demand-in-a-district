"""
@author: Fernando Penaherrera @UOL/OFFIS and Henrik Wagner @elenia/TUBS

This file contains all necessary functions to prepare the data for the co-simulation. 
It only needs to be run once for every co-simulation configuration.
Example when data preparation is activated:
    configuration = {
            ...
            "overwrite_data": True,
            ...
"""
import datetime
import glob
import json
from logging import raiseExceptions
import logging
import math
import os
from os.path import join, isfile
import random
import warnings

# from dwd_data_downloader.dwd_downloader import meteo_data_downloader
from pandas.core.common import SettingWithCopyWarning
from pvlib._deprecation import pvlibDeprecationWarning
from pvlib.irradiance import erbs
from pvlib.location import Location
from pvlib.modelchain import ModelChain
from pvlib.pvsystem import PVSystem as PVLibSystem
from pvlib.temperature import TEMPERATURE_MODEL_PARAMETERS

import pandapower as pp
import pandas as pd
from src.common import (
    DATA_PROCESSED_DIR,
    DATA_RAW_DIR,
    DATA_CAR_DIR,
    DATA_PV_DIR,
    DATA_LOAD_DIR,
    Scenarios,
)
from src.preprocessing.write_csv import write_csv_files


logging.basicConfig(level=logging.DEBUG)
logging.basicConfig(format="%(asctime)s %(message)s")
warnings.filterwarnings("ignore", category=DeprecationWarning)
warnings.filterwarnings("ignore", category=pvlibDeprecationWarning)
warnings.filterwarnings("ignore", category=SettingWithCopyWarning)
warnings.filterwarnings("ignore", category=pd.errors.PerformanceWarning)

GRID_DATA_FILE = join(DATA_RAW_DIR, "grid_topology.xlsx")


def create_household_data(houses=65, units_per_house=6, year=2020):
    """Compose a CSV file with information on the loads

    Args:
        houses (int, optional): Number of houses. Defaults to 65.
        units_per_house (int, optional): Number of units (apartments) per house. Defaults to 6.
        year (int, optional): Year of the timeseries. Defaults to 2020.

    Returns:
        string: Path of the created data
    """
    raw_filename = "Gesamtverbrauchsdaten.csv"
    # Read csv file as dataframe

    raw_data_path = join(DATA_RAW_DIR, raw_filename)

    df = pd.read_csv(raw_data_path, sep=";", parse_dates=["rec_time"])

    # Delete the column "Einheit"
    del df["Einheit"]

    # Time increment
    START = f"{year}-01-01 00:00:00"
    END = f"{year}-12-31 23:45:00"
    dti = pd.date_range(start=START, end=END, freq="15T")

    steps = len(dti)
    delta = df.iloc[1, 0] - df.iloc[0, 0]
    delta_t = delta.total_seconds() / 3600  # seconds to hour
    df.set_index("rec_time", inplace=True)

    # Convert kWh to W, scaling to the time resolution, from
    df_w = df / (delta_t) * 1000
    rows_drop = df_w.shape[0] - steps  # Number of rows to drop

    # limiting the data to one year
    # Dropping last n rows using drop
    df_w.drop(df_w.tail(rows_drop).index, inplace=True)

    df_w["Time"] = dti
    df_w.set_index("Time", inplace=True)

    # Creating random columns: create a dataframe with all Households in
    # Oelper Berge (65*6 HH = 390 HH) by selecting random columns in df_w
    n_samples = houses * units_per_house
    hh_all = df_w.sample(n_samples, axis="columns", replace=True, random_state=123456)

    # new header is also random, therefore remove header to avoid
    # misunderstanding
    hh_all.columns = range(hh_all.shape[1])

    # Group and sum every 6 columns
    hh_all_grouped = hh_all.groupby(
        [[math.floor(i / units_per_house) for i in range(0, n_samples)]], axis=1
    ).sum()

    # Rename headers
    hh_all_grouped.columns = [f"House{i}" for i in range(0, houses)]
    hh_all_grouped.index.rename("Time", inplace=True)

    # Save in CSV format
    csv_filename = join(DATA_LOAD_DIR, "Household_Loads.csv")
    hh_all_grouped.to_csv(csv_filename)
    logging.info(f"Household data saved to {csv_filename}")
    return "Household_Loads.csv"


def create_separate_profiles(filename, mode, skiprows, year):

    if mode == "load":
        # read-in csv which needs to be separated
        path = join(DATA_LOAD_DIR, filename)

    elif mode == "pv":
        # read-in csv which needs to be separated
        path = join(DATA_PV_DIR, filename)

    else:
        raise Warning("Wrong mode to create separate profiles")

    # read-in csv-files
    df = pd.read_csv(path, skiprows=skiprows)

    # create dict to create dataframes
    d = {}
    for df_name in df.columns:
        d[df_name] = pd.DataFrame()

    # delete entry for Time+
    if "Time" in d:
        del d["Time"]

    # create dict for loads to save in .json
    export = []

    # set start_date for .json
    start_date = f"{year}-01-01 00:00:00"

    # loop over all houses
    for i in range(1, len(df.columns)):

        # read name of current house
        name = df.columns[i]
        d[i] = df[df.columns[[0, i]]]

        if mode == "load":
            # export dataframe consisting of one house to new .csv
            path_export = f"{DATA_LOAD_DIR}\\Loadprofile_{name}.csv"

        if mode == "pv":
            # export dataframe consisting of one pv to new .csv
            path_export = f"{DATA_PV_DIR}\\PVprofile_{name}.csv"

        # export all columns (loads or pv) to separate .csv
        d[i].to_csv(path_export, index=False)

        # create dict_export for current household / PV profile
        dict_csv = {
            "datafile": path_export,
            "date_format": "YYYY-MM-DD HH:mm:ss",
            "header_rows": 1,
            "start_date": start_date,
        }
        export.append(dict_csv)

    return export


def create_pv_sizing(pv_excel_data: str, pv_scaling=1):
    """Creates a dictionary with the parameters of the PV

    Args:
        pv_excel_data (str): Filename with the information on PV Sizing and system configuration
        pv_scaling (int, optional): Scaling of the PV (for experimentation). Defaults to 1.

    Returns:
        str: Dataframe with the sorted data.
    """
    pv_data = pd.read_excel(pv_excel_data, "dict", index_col="bus", skiprows=range(1, 2))
    pv_data = pv_data.sort_index()

    if pv_scaling:
        pv_data["p_ac"] *= pv_scaling
        pv_data["p_dc"] *= pv_scaling

    pv_data = pv_data[
        [
            "albedo",
            "elevation",
            "inclination",
            "latitude",
            "longitude",
            "surface_azimuth",
            "timezone",
            "p_ac",
            "p_dc",
            "inverter_eff",
        ]
    ]

    pv_json_dir = join(DATA_PV_DIR, "pv_sizing.json")
    pv_data.to_json(pv_json_dir, orient="index")
    logging.info(f"PV Sizing Dictionaries saved to {pv_json_dir}")

    return pv_data


def create_pv_csv_data(pv_data, meteo_data_path=None):
    """Creates a CSV with the timeseries of PV Production

    Args:
        pv_data (pd.Dataframe): A dataframe with the description of the PV System
        meteo_data_path (str, optional): Path to the Meteo Data file. Defaults to None.

    Returns:
        str: Path of the created CSV Timeseries
    """
    # Get a weather Dataframe
    if meteo_data_path is None:
        pv_filename = "Braunschweig_meteodata_2020_15min.csv"
        meteo_data_path = join(DATA_PROCESSED_DIR, pv_filename)

    weather = pd.read_csv(meteo_data_path, parse_dates=True, index_col="Time")

    # Create a dataframe for future saving
    pv_results = pd.DataFrame(index=weather.index)

    # Calculate pv power
    for key, val in pv_data.iterrows():
        val["temp_model_params"] = TEMPERATURE_MODEL_PARAMETERS["sapm"]["open_rack_glass_polymer"]

        location = Location(
            latitude=val["latitude"], longitude=val["longitude"], altitude=val["elevation"]
        )

        zenith = location.get_solarposition(
            times=weather.index, temperature=weather["AirTemperature"]
        )
        missing_radiation_pars = erbs(weather["GlobalRadiation"], zenith["zenith"], weather.index)
        dhi = missing_radiation_pars["dhi"]
        dni = missing_radiation_pars["dni"]
        weather_df = weather[["GlobalRadiation", "AirTemperature", "WindSpeed"]]
        weather_df["dni"] = dni
        weather_df["dhi"] = dhi

        # PV Watts requires specific column names
        weather_df.rename(
            columns={
                "GlobalRadiation": "ghi",
                "AirTemperature": "temp_air",
                "WindSpeed": "wind_speed",
            },
            inplace=True,
        )

        # Create a PVSystem based on the configuration
        module_params = dict(pdc0=val["p_dc"], gamma_pdc=-0.003)

        p_ac = val["p_ac"]
        inverter_eff = val["inverter_eff"]
        inverter_params = dict(pdc0=p_ac / inverter_eff)

        system = PVLibSystem(
            surface_tilt=val["inclination"],
            surface_azimuth=val["surface_azimuth"],
            module_parameters=module_params,
            inverter_parameters=inverter_params,
            temperature_model_parameters=val["temp_model_params"],
        )

        model_chain = ModelChain.with_pvwatts(system=system, location=location)

        model_chain.run_model(weather_df)
        pv_results[f"PV{key}"] = model_chain.results.ac

    pv_res_filename = join(DATA_PV_DIR, "pv_production.csv")
    pv_results.to_csv(pv_res_filename)
    logging.info(f"PV Production data saved to {pv_res_filename}")

    return pv_results


def create_pv_data(meteo_data_path, pv_scaling=None):
    """Chaining the two functions to a single pipeline

    Args:
        meteo_data_path (string): Path to the Meteo Data File

    Returns:
        str: Path of the created CSV Timeseries
    """
    pv_excel_data = join(DATA_RAW_DIR, "PV_sizing.xlsx")
    pv_data = create_pv_sizing(pv_excel_data, pv_scaling=pv_scaling)

    if meteo_data_path is None:
        meteo_data_path = join(DATA_PROCESSED_DIR, "Braunschweig_meteodata_2020_15min.csv")
    pv_results = create_pv_csv_data(pv_data, meteo_data_path)

    return pv_results


def create_grid(grid_file_name=GRID_DATA_FILE):
    """Creates a grid for a district with the information in the Excel file
    Saves to a json file in the current folder

    Args:
        grid_file_name (str, optional): Excel file name with the topology of the grid. Defaults to GRID_DATA_FILE.

    Returns:
        pandapower.net: A pandapower net object with the grid.
    """

    # Create empty grid
    net = pp.create_empty_network()

    # Create Buses
    df_bus = pd.read_excel(grid_file_name, sheet_name="bus", index_col=0)
    for idx in df_bus.index:
        pp.create_bus(
            net,
            vn_kv=df_bus.at[idx, "vn_kv"],
            name=df_bus.at[idx, "name"],
            geodata=((df_bus.at[idx, "x"]), (df_bus.at[idx, "y"])),
            type=df_bus.at[idx, "type"],
            in_service=df_bus.at[idx, "in_service"],
        )

    # Create loads
    df_load = pd.read_excel(grid_file_name, sheet_name="load", index_col=0)
    for idx in df_load.index:
        pp.create_load(
            net,
            bus=df_load.at[idx, "bus"],
            p_mw=df_load.at[idx, "p_mw"],
            name=df_load.at[idx, "name"],
            scaling=df_load.at[idx, "scaling"],
        )

    # Create external grids
    df_ext_grid = pd.read_excel(grid_file_name, sheet_name="externalgrid", index_col=0)
    for idx in df_ext_grid.index:
        pp.create_ext_grid(
            net,
            bus=df_ext_grid.at[idx, "bus"],
            vm_pu=df_ext_grid.at[idx, "vm_pu"],
            name=df_ext_grid.at[idx, "name"],
        )

    # CREATE NON-STANDARD LINES
    df_line = pd.read_excel(grid_file_name, sheet_name="line", index_col=0)

    for idx in df_line.index:
        pp.create_line_from_parameters(
            net,
            name=df_line.at[idx, "name"],
            from_bus=df_line.at[idx, "from_bus"],
            to_bus=df_line.at[idx, "to_bus"],
            length_km=df_line.at[idx, "length_km"],
            r_ohm_per_km=df_line.at[idx, "r_ohm_per_km"],
            x_ohm_per_km=df_line.at[idx, "x_ohm_per_km"],
            c_nf_per_km=df_line.at[idx, "c_nf_per_km"],
            g_us_per_km=df_line.at[idx, "g_us_per_km"],
            max_i_ka=df_line.at[idx, "max_i_ka"],
        )

    # CREATE STANDARD TRAFOS
    df_trafo = pd.read_excel(grid_file_name, sheet_name="trafo", index_col=0)
    for idx in df_trafo.index:
        pp.create_transformer(
            net,
            hv_bus=df_trafo.at[idx, "hv_bus"],
            lv_bus=df_trafo.at[idx, "lv_bus"],
            std_type=df_trafo.at[idx, "std_type"],
        )

    # CREATE SGENS as PV
    df_pv = pd.read_excel(grid_file_name, sheet_name="sgen", index_col=0)
    for idx in df_pv.index:
        pp.create_sgen(
            net,
            bus=df_pv.at[idx, "bus"],
            p_mw=df_pv.at[idx, "p_mw"],
            q_mvar=df_pv.at[idx, "q_mvar"],
            name=df_pv.at[idx, "name"],
            scaling=df_pv.at[idx, "scaling"],
            type=df_pv.at[idx, "type"],
            in_service=True,
        )

    # CREATE STORAGE as
    df_sto = pd.read_excel(grid_file_name, sheet_name="storage", index_col=0)

    # Momentary real power of the storage
    # (positive for charging, negative for discharging)
    for idx in df_sto.index:
        pp.create_storage(
            net,
            bus=df_sto.at[idx, "bus"],
            p_mw=df_sto.at[idx, "p_mw"],
            q_mvar=df_sto.at[idx, "q_mvar"],
            name=df_sto.at[idx, "name"],
            scaling=df_sto.at[idx, "scaling"],
            max_e_mwh=df_sto.at[idx, "max_e_mwh"],
            min_p_mw=df_sto.at[idx, "min_p_mw"],
            max_p_mw=df_sto.at[idx, "max_p_mw"],
            in_service=True,
        )

    # Charging stations are modelled as extra loads.
    # Positive for grid consumption/EV charging
    # Negative for grid insertion/EV discharging
    df_cs = pd.read_excel(grid_file_name, sheet_name="cs", index_col=0)
    for idx in df_cs.index:
        # we are aming at around 144 cars because there are 144 available parking spaces
        # So I create 3 extra loads per bus
        # for i in range(0, 3):
        pp.create_load(
            net,
            bus=df_cs.at[idx, "bus"],
            p_mw=df_cs.at[idx, "p_mw"],
            name=df_cs.at[idx, "name"],
            scaling=df_cs.at[idx, "scaling"],
            in_service=True,
        )

    grid_name = join(DATA_PROCESSED_DIR, "Grid_Model.json")
    pp.to_json(net, grid_name)

    logging.info(f"Grid file saved to {grid_name}")

    return net


def resample_data(filename: str, mode: str, path: str, mins=15):
    """Resamples the data from 1h to 15 mins resolution
    Args:
        filename (str): Original file to be resampled. Must be a CSV with timeseries
        path (str, optional): Path of the file. Defaults to None.
        mins (int, optional): Minutes for resampling. Defaults to 15.

    Returns:
        str: Path of the resampled timeseries.
    """

    full_filename = join(path, filename)
    # Some files start at 0, others at 1
    try:
        df = pd.read_csv(full_filename, skiprows=1, index_col="Time", parse_dates=True)
    except:
        df = pd.read_csv(full_filename, skiprows=0, index_col="Time", parse_dates=True)
    df_resampled = pd.DataFrame()

    for c in df.columns:
        series = df[c]
        series_down = series.resample(f"{mins}min")
        series_int = series_down.interpolate(method="linear")
        df_resampled[c] = series_int

    new_filename = filename[0:-4] + f"_{mins}min.csv"
    if mode == "weather":
        new_filename_full = join(DATA_PROCESSED_DIR, new_filename)
    elif mode == "load":
        new_filename_full = join(DATA_LOAD_DIR, new_filename)
    df_resampled.to_csv(new_filename_full)

    logging.info(f"Data resampled to {new_filename_full}")
    return new_filename_full


def create_car_data(
    pickle_resolution,
    worker_type,
    start_date_time="2020-01-01 00:00:00",
    soc_init=0.5,
    randomize=True,
    end_time_s=86400,
):
    """Creates the data set for the district's EVs based on input data (pickle-files) from emobpy.
    Needed to generate the model_data_car_cs.json.

    Args:
        pickle_resolution (str): Input (1min/15min) to select the pickle-files accordingly to simulation step size.
        worker_type (str): Choice of the underlying type of commuter (fulltime, parttime, freetime)
        start_date_time (str, optional): Sets the start_date_time of data set in accordance to choicen simulation time.
        Should mostly be left on default. Defaults to "2020-01-01 00:00:00".
        soc_init (float, optional): Initial SoC of the EVs. Defaults to 0.5.
        randomize (bool, optional): Option to choose random emobpy pickle-files for data generation
        based on input worker type (multiple emobpy pickle-files for each worker type available). Defaults to True.
        end_time_s (int, optional): Set end time of data set to end of simulation. Defaults to 86400.

    Returns:
        dict: Returns the dict with the chosen emobpy pickle-files to generate the model_data_car_cs.json file.
    """

    # Based on worker_type choice a random car profile is chosen
    car_path = DATA_CAR_DIR
    if pickle_resolution == "1min":
        os.chdir(join(car_path, pickle_resolution))
    elif pickle_resolution == "15min":
        os.chdir(join(car_path, pickle_resolution))
    else:
        raiseExceptions("Chosen resolution of car.pickle files not existing")

    if randomize == True:
        car_profile_list = []
        freetime_profiles = []
        parttime_profiles = []
        fulltime_profiles = []
        for file in glob.glob("*.pickle"):
            car_profile_list.append(file)
        for item in car_profile_list:
            if "freetime" in item:
                freetime_profiles.append(item)
            if "parttime" in item:
                parttime_profiles.append(item)
            if "fulltime" in item:
                fulltime_profiles.append(item)
        if worker_type == "freetime":
            file_emobpy = random.choice(freetime_profiles)
        elif worker_type == "parttime":
            file_emobpy = random.choice(parttime_profiles)
        elif worker_type == "fulltime":
            file_emobpy = random.choice(fulltime_profiles)
        else:
            # Handing over specific shares of worker types not yet implemented
            raiseExceptions(
                "Pre-defined distribution of worker types to generate mobility behavior are needed"
            )

    else:
        raiseExceptions("Handing over specific profiles not yet implemented")
        # file_emobpy = input("Please enter the name of the car profile that you wish to simulate(.pickle file ): \n")

    date = start_date_time.rsplit(" ")[0]
    # checking if format matches the date
    res = True
    # using try-except to check for truth value
    try:
        res = bool(datetime.datetime.strptime(date, "%Y-%m-%d"))
    except ValueError:
        print("Wrong date format please follow given instructions")
        res = False
    if res == True:
        year = start_date_time.rsplit("-", 2)
        if year[0] != "2020":
            year[0] = "2020"
            start_date_time = "-".join(year)

    dict_car = {
        "file_emobpy": os.path.join(DATA_CAR_DIR, pickle_resolution, file_emobpy),
        # "data\\car\\" + pickle_resolution + "\\" + file_emobpy,
        "date_format": "YYYY-MM-DD HH:mm:ss",
        "soc_init": soc_init,
        "E_bat_rated": 60000,
        "start_time": start_date_time,
        "END": end_time_s,
    }
    return dict_car


def create_cs_data(randomize=False, P_rated_cs=11000):
    """Generates the data set for the charging stations within the district.
    Needed to generate the model_data_car_cs.json.

    Args:
        randomize (bool, optional): Option to ramdomize the choice of charging strategy.
        Later overwritten by simulation scenario. Defaults to False.
        P_rated_cs (int, optional): Rated charging power of the charging stations.
        Defaults to 11000 W.

    Returns:
        dict: Returns dict with data set of charging stations which is used to
        generate the model_data_car_cs.json file.
    """

    available_strategy = ["max_P", "forecast", "night_charging", "solar_charging"]
    if randomize == True:
        strategy = random.choice(available_strategy)
    else:
        strategy = "max_P"

    dict_cs = {"strategy": strategy, "output_type": "AC", "P_rated": P_rated_cs}
    return dict_cs


def create_storage_data(e_bat_rated, randomize=False, charging_limit=None, status_curve=False):
    """Creates the data set for the battery storage models. Needed to create model_data_storage.json

    Args:
        e_bat_rated (int): Rated battery capacity [Wh]
        randomize (bool, optional): Option for randomization of battery operation
        strategy (uncontrolled/minimize curtailment). Defaults to False.
        charging_limit (int, optional): For operation strategy: minimize_curtailment.
        Percentage to which battery charging power is reduced to avoid curtailment of PV. Defaults to None.
        status_curve (bool, optional): Option to enable dynamic charging/discharging efficiencies
        (replicated based on measured efficiency) curves of battery inverters). Defaults to False.

    Returns:
        dict: Returns dict with data set of battery storage which is used to generate model_data_storage.json
    """

    op_strategy = ["uncontrolled", "minimize_curtailment"]
    if randomize == True:
        operation_strategy = random.choice(op_strategy)
    else:
        operation_strategy = "uncontrolled"  # "minimize_curtailment" #

    dict_sto = {
        "E_bat_rated": e_bat_rated,
        "eta_BAT2AC": [-0.000623, -1.65, 0.9591],
        "eta_AC2BAT": [-0.0006864, -1.641, 0.957],
        "status_curve": status_curve,
        "status_aging": True,
        "operation_strategy": operation_strategy,
        "charging_limit": charging_limit,
    }
    return dict_sto


def write_json(data, filename):
    """Writes input data/dicts into model_data_<filename>.json file.

    Args:
        data (dict): Input dict for .json file (e.g. dict_sto, dict_car)
        filename (str): Name of the generated .json file (e.g. model_data_car_cs.json)
    """
    with open(filename, "w") as f:
        json.dump(data, f, indent=4)


def create_car_and_cs_json(
    no_of_cars,
    no_of_cs,
    pickle_resolution,
    randomize=False,
    start_date_time="2020-01-01 00:00:00",
    filename="model_data_car_cs.json",
    P_rated_cs=11000,
):
    """Generates the input data file (model_data_car_cs.json) for the mosaik-car and mosaik-cs model
    defining their input values.

    Args:
        no_of_cars (int): Number of cars to be generated based on configuration in main.py
        no_of_cs (_type_): Number of charging stations to be generated.
        pickle_resolution (_type_): Input resoultion of emobpy pickle-files (1min/15min).
        randomize (bool, optional): Option to randomize choice of worker types for
        selected emobpy pickle-files. Defaults to False, setting it in accordance to literature reference.
        start_date_time (str, optional): _description_. Defaults to "2020-01-01 00:00:00".
        filename (str, optional): _description_. Defaults to "model_data_car_cs.json".
        P_rated_cs (int, optional): _description_. Defaults to 11000.
    """

    worker_type = []
    list_worker_types = ["fulltime", "parttime", "freetime"]
    car = []  # Empty list to store all cars
    cs = []  # Empty list to store all charging stations
    # Enter the worker type for each car from no_of_cars
    if randomize == True:
        for i in range(no_of_cars):
            worker_type.append(random.choice(list_worker_types))

        for i in range(0, no_of_cars):
            car.append(
                create_car_data(
                    worker_type=worker_type[i],
                    start_date_time=start_date_time,
                    soc_init=1,
                    pickle_resolution=pickle_resolution,
                )
            )
        for i in range(0, no_of_cs):
            cs.append(create_cs_data(randomize=False, P_rated_cs=P_rated_cs))

    # Generate car data based on fixed distribution of worker types
    else:
        """Fixed distribution of worker types based on OECD.
        Commuter: 62%; Non-commmuter 38%; Further separation of: 78% fulltime; 22% parttime
        TOTAL: 48,35% fulltime; 13,65% parttime; 38% freetime
        OECD Labour Force Statistics 2018. https://doi.org/10.1787/oecd_lfs-2018-en (2018)
        """

        # Specific configuration for "Am Ölper Berge" initical scenario
        if no_of_cars == 49 and no_of_cs == 49:
            fulltime = 24
            parttime = 7
            freetime = 18

        else:
            # OECD distribution of worker types
            fulltime = int(round(no_of_cars * 0.4835, 0))  # 49EVs: 24
            parttime = int(round(no_of_cars * 0.1365, 0))  # 49EVs: 7
            freetime = int(round(no_of_cars * 0.38, 0))  # 49EVs: 18

        # fill list for worker types
        for i in range(fulltime):
            worker_type.append("fulltime")
        for i in range(parttime):
            worker_type.append("parttime")
        for i in range(freetime):
            worker_type.append("freetime")

        # shuffle worker_type list for a realistic distribution in district / electric grid
        random.shuffle(worker_type)

        # check for rounding error
        if len(worker_type) != no_of_cars:
            print(
                "Error in distribution of worker types. \
            Please adjust distribution manuelly so it fits to number of EVs generated"
            )

        else:
            for i in range(0, no_of_cars):
                car.append(
                    create_car_data(
                        worker_type=worker_type[i],
                        start_date_time=start_date_time,
                        randomize=True,
                        soc_init=1,
                        pickle_resolution=pickle_resolution,
                    )
                )
            for i in range(0, no_of_cs):
                cs.append(create_cs_data(randomize=True, P_rated_cs=P_rated_cs))

    # Writing to .json
    data = {
        "car": car,
        "charging_station": cs,
    }
    os.chdir(DATA_PROCESSED_DIR)
    write_json(data, filename=filename)
    logging.info(f"Car and CS file saved to {join(DATA_PROCESSED_DIR,filename)}")


def create_storage_json(
    household_data_filename,
    no_of_storages=49,
    dimensioning_ratio=1.0,
    randomize=False,
    filename="model_data_storage.json",
):
    """Creates the .json for battery storage models (model_data_storage.json)

    Args:
        household_data_filename (str): Filename of the household load profiles as
        no_of_storages (int, optional): Number of storages to be generated. Defaults to 49.
        dimensioning_ratio (float, optional): Ratio of installed nominal power of PV systems
        to battery capacity. Defaults to 1.0 (1 kWh battery /1 kWp PV).
        randomize (bool, optional): Handover value for randomized choice of battery operation
        strategy. Defaults to False.
        filename (str, optional): Name of .json file to be generated. Defaults to "model_data_storage.json".
    """

    storage = []
    Jahresverbrauch = []
    df = pd.read_csv(join(DATA_LOAD_DIR, household_data_filename), header=0, low_memory=False)
    for i in range(0, no_of_storages):
        sum_load = df["House{}".format(i)].sum()
        Jahresverbrauch.append(sum_load * 15 / 60000)
    for i in range(0, no_of_storages):
        Jahresverbrauch.append((sum(df[f"House{i}"])) * 15 / 60000)
    for i in range(0, no_of_storages):
        storage.append(
            create_storage_data(
                dimensioning_ratio * math.ceil(Jahresverbrauch[i]),
                randomize,
                charging_limit=0.6,
                status_curve=False,
            )
        )

    storage = {
        "storage": storage,
    }
    os.chdir(DATA_PROCESSED_DIR)
    write_json(storage, filename=filename)
    logging.info(f"Storage file saved to {join(DATA_PROCESSED_DIR,filename)}")


def prepare_simulation_data(
    scenario=Scenarios.TEST,
    overwrite=True,
    no_of_houses=49,
    no_of_storages=49,
    no_of_cars=49,
    no_of_cs=49,
    pickle_resolution="15min",
):
    """Collection of methods for preparing the required simulation data

    Args:
        scenario (Scenarios.Enum): One of the Scenarios
        overwrite (bool, optional): Overwrite the current data files. Defaults to True.
    """
    year = 2020
    # scaling_load = 1
    scaling_pv = 1
    # scaling_battery = 1
    # scaling_ev = 1

    # All of the scenarios use meteo_data
    scenario_model_data = {}

    location = {"name": "Braunschweig", "id": "BS", "lat": 52.29, "lon": 10.44}

    meteo_data_path = join(DATA_RAW_DIR, f"{location['name']}_meteodata_{year}.csv")

    if not isfile(meteo_data_path):  # or overwrite: This file shall always be there
        meteo_data_path = meteo_data_downloader(
            location=location, year=year, path=DATA_RAW_DIR, delete_temp_files=True
        )

    # Weather data resampling
    filename = join(DATA_PROCESSED_DIR, f"Braunschweig_meteodata_{year}_15min.csv")
    if not isfile(filename) or overwrite:
        weather_filename = f"Braunschweig_meteodata_{year}.csv"
        weather_filename_15mins = resample_data(
            filename=weather_filename, path=DATA_RAW_DIR, mode="weather"
        )

    else:
        weather_filename_15mins = filename
        logging.info(f"File exists: {filename}")
    scenario_model_data["meteo_data_path"] = filename

    # Grid JSON file
    filename = join(DATA_PROCESSED_DIR, "Grid_Model.json")
    if not isfile(filename) or overwrite:
        create_grid()
    else:
        logging.info(f"File exists: {filename}")
    scenario_model_data["grid_file_path"] = filename

    # Household loads in 15min resolution and separate profiles
    filename = join(DATA_LOAD_DIR, "Household_Loads_15min.csv")
    if not isfile(filename) or overwrite:
        household_data_filename = create_household_data(
            houses=no_of_houses, units_per_house=6, year=2020
        )
        household_data_15_min_filename = resample_data(
            filename=household_data_filename, path=DATA_LOAD_DIR, mode="load"
        )

        # create separate .csv profiles from household_data_15_min_filename
        if isfile(join(DATA_LOAD_DIR, household_data_15_min_filename)):
            loads = []
            loads = create_separate_profiles(
                household_data_15_min_filename, mode="load", skiprows=0, year=2020
            )

            # create model_data_household.jsons
            filename = "model_data_household.json"
            scenario_model_data["household_json_path"] = join(DATA_PROCESSED_DIR, filename)

            # Writing to .json
            data = {
                "load": loads,
            }
            os.chdir(DATA_PROCESSED_DIR)
            write_json(data, filename=filename)
            logging.info(
                f"Household file (seperated profiles) saved to {join(DATA_PROCESSED_DIR,filename)}"
            )
        else:
            raiseExceptions(
                f"{household_data_15_min_filename} which needs to be separated does not exist!"
            )

    else:
        logging.info(f"File exists: {filename}")

    # set scenario_model_data paths
    scenario_model_data["household_json_path"] = join(
        DATA_PROCESSED_DIR, "model_data_household.json"
    )
    scenario_model_data["load_data_path"] = join(DATA_LOAD_DIR, "Household_Loads_15min.csv")

    # PV Production CSV
    if scenario in [Scenarios.EV_PV, Scenarios.EV_PV_STO, Scenarios.GRID_OBSV]:
        filename = join(DATA_PV_DIR, "pv_production.csv")
        if not isfile(filename) or overwrite:
            create_pv_data(meteo_data_path=weather_filename_15mins, pv_scaling=scaling_pv)

            # create separate .csv profiles from pv_production.csv for run_prognosis status
            if isfile(join(DATA_PV_DIR, "pv_production.csv")):
                pvs = []
                pvs = create_separate_profiles(
                    "pv_production.csv", mode="pv", skiprows=0, year=2020
                )

                # create model_data_pv.jsons
                filename = "model_data_pv_csv.json"
                scenario_model_data["pv_data_csv_path"] = join(DATA_PV_DIR, "pv_production.csv")

                # Writing to .json
                data = {
                    "pv": pvs,
                }
                os.chdir(DATA_PROCESSED_DIR)
                write_json(data, filename=filename)
                logging.info(f"PV files (seperated profiles) saved to {join(DATA_PV_DIR,filename)}")
            else:
                raiseExceptions("pv_production.csv which needs to be separated does not exist!")

        else:
            logging.info(f"File exists: {filename}")
        scenario_model_data["pv_data_path"] = filename
        scenario_model_data["pv_json_data_path"] = join(DATA_PV_DIR, "pv_sizing.json")
        scenario_model_data["pv_data_csv_path"] = join(DATA_PROCESSED_DIR, "model_data_pv_csv.json")

    # Battery Storage .json
    if scenario in [Scenarios.EV_PV_STO, Scenarios.GRID_OBSV]:
        filename = join(DATA_PROCESSED_DIR, "model_data_storage.json")
        if not isfile(filename) or overwrite:
            create_storage_json(household_data_filename, no_of_storages=no_of_storages)

        else:
            logging.info(f"File exists: {filename}")
        scenario_model_data["storage_json_path"] = join(
            DATA_PROCESSED_DIR, "model_data_storage.json"
        )

    # EV and CS .json
    if scenario in [Scenarios.EV, Scenarios.EV_PV, Scenarios.EV_PV_STO, Scenarios.GRID_OBSV]:

        filename = join(DATA_PROCESSED_DIR, "model_data_car_cs.json")
        if not isfile(filename) or overwrite:
            create_car_and_cs_json(
                no_of_cars=no_of_cars,
                no_of_cs=no_of_cs,
                pickle_resolution=pickle_resolution,
                P_rated_cs=11000,
            )

        else:
            logging.info(f"File exists: {filename}")
        scenario_model_data["car_cs_json_path"] = join(DATA_PROCESSED_DIR, "model_data_car_cs.json")

    # The Mosaik CSV Format needs a header.
    write_csv_files(scenario_model_data["meteo_data_path"], location["name"])
    write_csv_files(scenario_model_data["load_data_path"], header="HouseholdLoads")

    # if scenario in [Scenarios.EV_PV, Scenarios.EV_PV, Scenarios.EV_PV_STO, Scenarios.GRID_OBSV]:
    #     write_csv_files(scenario_model_data['pv_data_path'], header="PV")

    return scenario_model_data


if __name__ == "__main__":
    """Function to test functionality of data preparation."""
    prepare_simulation_data(
        scenario=Scenarios.EV_PV_STO,
        overwrite=True,
        no_of_houses=49,
        no_of_storages=49,
        no_of_cars=49,
        no_of_cs=49,
        pickle_resolution="15min",
    )

    # create_car_and_cs_json(no_of_cars=50, no_of_cs=50, pickle_resolution="15min")
