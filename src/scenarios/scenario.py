"""
@author: Fernando Penaherrera @UOL/OFFIS and Henrik Wagner @elenia/TUBS

The co-simulation scenario and resulting mosaik-simulators/-models is automatically 
built based on the configuration in main.py

=================================
Symbols for the pandapower grid:
PV System Generation: Positive
PV System Consuming: Negative (inverter consuming in idle mode)
Storage Charging: Positive
Storage Discharging: Negative
House Load: Positive
EV-Charging Station Charging: Positive
=================================

"""
from src.scenarios.scenario_config import SIM_CONFIG
import random
import mosaik
import logging
from src.common import Scenarios, RESULTS_DB_DIR, RESULTS_POSTPROC_DIR
from os.path import join
import json
from mosaik.util import connect_many_to_one
from src.postprocessing.database_analysis import analyze_database
from src.preprocessing.prepare_data import prepare_simulation_data
import math

logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)


def create_world(configuration):
    """
    Creation of the main instance for cosimulation
    """
    random.seed(123456)
    world = mosaik.World(SIM_CONFIG)
    logging.info("Mosaik World Object Created")
    return world


def configure_scenario(world, configuration, scenario=Scenarios.TEST):
    """
    Configuration of the scenario

    :param world: mosaik.world object
    :param configuration: Dictionary with the configuration parameters.
    :param scenario: One of the scenarios in the Scenarios(Enum)
    :param create_data: True if the data has to be fully replaced

    Example::

        configuration = {
            "start": '2020-01-01 00:00:00',
            "end": 24 * 3600 * 2,
            "step_size": 60 * 15,
            "overwrite_data": False}

        world = create_world(configuration)
        world = configure_scenario(world,
                                   configuration,
                                   scenario=Scenarios.SIMPLE,
                                   create_data=True)

    """
    pv = False
    grid_obs = False
    storage = False
    control = False
    car = False
    cs = False

    # Set bools for components depending on chosen scenario
    if scenario in [
        Scenarios.EV,
        Scenarios.EV_PV,
        Scenarios.EV_PV,
        Scenarios.EV_PV_STO,
        Scenarios.GRID_OBSV,
    ]:
        car = True
        cs = True

    if scenario in [
        Scenarios.EV_PV,
        Scenarios.EV_PV,
        Scenarios.EV_PV_STO,
        Scenarios.GRID_OBSV,
    ]:
        pv = True

    if scenario in [Scenarios.EV_PV_STO, Scenarios.GRID_OBSV]:
        storage = True
        control = True

    if scenario in [Scenarios.GRID_OBSV]:
        grid_obs = True

    # Configure the different components
    START = configuration["start"]
    END = configuration["end"]
    STEP_SIZE = configuration["step_size"]
    OVERWRITE_DATA = configuration["overwrite_data"]
    STATUS = configuration["status"]
    N_CARS = configuration["n_cars"]
    CHARGING_STRATEGY = configuration["charging_strategy"]

    logging.info(f"Working on scenario {scenario.name} with {N_CARS} EVs")
    logging.info(f"Charging Strategy is set to {CHARGING_STRATEGY}")

    # The max amount of cars is Fixated to avoid resampling...
    # 6 EVs per building, 1 per house, as extreme case
    scenario_model_data = prepare_simulation_data(
        scenario=scenario,
        overwrite=OVERWRITE_DATA,
        no_of_cars=434,  # 49*6,
        no_of_cs=434,
    )

    grid_filename = scenario_model_data["grid_file_path"]
    load_filename = scenario_model_data["load_data_path"]
    meteo_filename = scenario_model_data["meteo_data_path"]
    household_filename = scenario_model_data["household_json_path"]

    # Load household loads
    with open(household_filename) as f:
        model_data_household = json.load(f)
        if len(model_data_household["load"]) != 49:
            raise ValueError('Number of households does not fit to "Am Ölper Berge" district')
        # update start_date of model_data_household
        else:
            for i in range(len(model_data_household["load"])):
                model_data_household["load"][i]["start_date"] = START
            logging.info(f"Start Date of model_data_household updated to {START}")

    if car and cs:
        # Reading created .json for car and cs data
        CAR_MODEL_DATA = scenario_model_data["car_cs_json_path"]
        with open(CAR_MODEL_DATA) as f:
            model_data_car_cs = json.load(f)
            if (
                len(model_data_car_cs["car"]) > 0
                and len(model_data_car_cs["charging_station"]) <= 0
            ):
                raise ValueError("At least one charging_station is needed for EV!")
        # Updating charging strategy from .json according to configuration
        if CHARGING_STRATEGY == "random":
            for i in range(len(model_data_car_cs["charging_station"])):
                model_data_car_cs["charging_station"][i]["strategy"] = random.choice(
                    ["max_P", "forecast", "night_charging", "solar_charging"]
                )
        else:
            for i in range(len(model_data_car_cs["charging_station"])):
                model_data_car_cs["charging_station"][i]["strategy"] = CHARGING_STRATEGY

    if pv:
        pv_json_filename = scenario_model_data["pv_json_data_path"]
        pv_data_filename = scenario_model_data["pv_data_path"]
        if STATUS == "run_prognosis" or STATUS == "test":
            with open(scenario_model_data["pv_data_csv_path"]) as f:
                model_data_pv_csv = json.load(f)
                if len(model_data_pv_csv["pv"]) != 49:
                    raise ValueError(
                        'Expected 49. Number of pv does not fit to "Am Ölper Berge" district'
                    )
                # update start_date of model_data_pv_csv
                else:
                    for i in range(len(model_data_pv_csv["pv"])):
                        model_data_pv_csv["pv"][i]["start_date"] = START
                    logging.info(f"Start Date of model_data_pv_csv updated to {START}")

    if storage:
        STORAGE_MODEL_DATA = scenario_model_data["storage_json_path"]
        with open(STORAGE_MODEL_DATA) as f:
            model_data_storage = json.load(f)

    database_file_name = join(
        RESULTS_DB_DIR, f"Results_{scenario.name}_{N_CARS}_{CHARGING_STRATEGY}.hdf5"
    )

    # Start the simulators ###################################################
    # csv_load_sim = world.start("mosaik-CSV", sim_start=START, datafile=load_filename)
    houses_sim = world.start("Load", step_size=STEP_SIZE)
    grid_sim = world.start("Pandapower", step_size=STEP_SIZE, mode="pf")
    db_sim = world.start("DB", step_size=STEP_SIZE, duration=END)

    if pv:
        # I use CSV data for testing. PVLIB is slowing down the model.
        if STATUS == "run":
            meteo_sim = world.start("mosaik-CSV", sim_start=START, datafile=meteo_filename)

            with open(pv_json_filename) as d:
                pv_json_data = json.load(d)

            pv_sim = world.start(
                "PVSim",
                start_date=START,
                step_size=STEP_SIZE,
                calc_mode="simple",
                pv_data=pv_json_data,
            )
        if STATUS == "test":
            pv_sim_csv = world.start("PV", step_size=STEP_SIZE)
            # csv_pv_sim = world.start("mosaik-CSV", sim_start=START, datafile=pv_data_filename)
            # pv_sim_load = world.start("PVSim", step_size=STEP_SIZE)

        if STATUS == "run_prognosis":
            pv_sim_csv = world.start("PV", step_size=STEP_SIZE)

    if cs and car:
        car_sim = world.start("CarSim", step_size=STEP_SIZE, duration=END)
        cs_sim = world.start("CSSim", step_size=STEP_SIZE)

    if control:
        control_sim = world.start("ControlSim", step_size=STEP_SIZE)

    if storage:
        storage_sim = world.start("StorageSim", step_size=STEP_SIZE)

    if grid_obs:
        grid_obs_sim = world.start("GridObsSim", step_size=STEP_SIZE)
        grid_corr_sim = world.start("GridCorrSim", step_size=STEP_SIZE)
        grid_node_sim = world.start("GridNodeSim", step_size=STEP_SIZE)

    # Instantiate the models #################################################

    # csv_load_model = csv_load_sim.HouseholdLoads.create(1)
    # houses_model = houses_sim.LoadSim.create(49, **{"name":  "Households"})
    # houses_model_entities = houses_sim.get_entities()
    # monitor = collector.Monitor()

    houses_model = houses_sim.household.create(
        len(model_data_household["load"]), init_vals=model_data_household["load"]
    )
    housesEntities = houses_sim.get_entities()

    grid_parent = grid_sim.Grid(gridfile=grid_filename)
    grid_model = grid_parent.children
    database_model = db_sim.Database(filename=database_file_name)

    if pv:
        if STATUS == "run":
            meteo_model = meteo_sim.Braunschweig.create(1)
            pv_model = pv_sim.PVSim.create(49)
            pv_model_entities = pv_sim.get_entities()
        if STATUS == "test" or STATUS == "run_prognosis":
            pv_model = pv_sim_csv.PV.create(
                num=len(model_data_pv_csv["pv"]), init_vals=model_data_pv_csv["pv"]
            )
            pv_model_entities = pv_sim_csv.get_entities()

    if storage:
        storages = storage_sim.eStorage.create(
            len(model_data_storage["storage"]), init_vals=model_data_storage["storage"]
        )
        storagesEntities = storage_sim.get_entities()

    if control:
        init_vals = None
        if grid_obs:
            if "soc_lims" in configuration.keys():
                init_vals = [configuration["soc_lims"]] * 49
            else:
                init_vals = [{"soc_min": 0.1, "soc_max": 0.9}] * 49
        # 49 BEM in ÄOB or len(houses_model)
        BEM = control_sim.BEM.create(49, init_vals=init_vals)
        BEM_control_entities = control_sim.get_entities()

    if grid_obs:
        grid_obs_model = grid_obs_sim.GridObserver.create(1)
        grid_corr_model = grid_corr_sim.GridCorrector.create(
            1, init_vals=[{"no_cars": N_CARS, "charging_strategy": CHARGING_STRATEGY}]
        )
        # this I have to automate with the grid buses
        grid_node_model = grid_node_sim.GridNode.create(49)
        grid_node_sim.add_controlled_storages(storagesEntities)

    if cs and car:
        # p#rint("creating cars: ", len(model_data_car_cs['car']))
        cars = car_sim.car.create(N_CARS, init_vals=model_data_car_cs["car"][0:N_CARS])
        cs = cs_sim.charging_station.create(
            N_CARS, init_vals=model_data_car_cs["charging_station"][0:N_CARS]
        )  # 49*3
        csEntities = cs_sim.get_entities()

    # connect car with charging station ##################################
    if cs and car:
        for i, car in enumerate(cars):
            if len(cs) - 1 >= i:
                world.connect(
                    car,
                    cs[i],
                    "appearance",
                    "E_BAT",
                    "E_BAT_MAX",
                    "P_DISCHARGE_MAX",
                    "P_CHARGE_MAX",
                    "STANDING_TIME",
                    "STANDING_TIME_START",
                    "STANDING_TIME_END",
                    "TIME",
                    "ONE_MIN_PICKLE",
                    "NEXT_STEP_STANDING_MINUTES",
                    "NEXT_STEP_MINUTES",
                    "SIGNAL_PROGNOSIS_EV_CS",
                    "BEV_consumption_period",
                    async_requests=True,
                )

    # Get grid elements ###################################################
    buses = [b for b in grid_model if b.type in "Bus"]
    loads = [l for l in grid_model if l.type in "Load" and "ext_load" not in l.eid]

    houseLoad = [h for h in loads if "house_" in h.eid]

    def sort_elements(list_els):
        sorted_dict = {}
        rang = len(list_els)
        prefix = list_els[0].eid.split("_")[0]  # "0-house"
        for i in range(rang):
            name = f"{prefix}_{i}"
            for el in list_els:
                if name == el.eid:
                    sorted_dict[i] = el

        sorted_list = [sorted_dict[i] for i in range(rang)]
        return sorted_list

    houseLoad = sort_elements(houseLoad)
    if pv:
        pv_gens = [s for s in grid_model if s.type in "Sgen" and "pv_" in s.eid]
        pv_gens = sort_elements(pv_gens)

    if storage:
        storageElements = [s for s in grid_model if s.type in "Storage" and "sto_" in s.eid]
        storageElements = sort_elements(storageElements)
    if cs:
        csLoads = [cs for cs in loads if "cs_" in cs.eid]
        csLoads = sort_elements(csLoads)

    trafo = [t for t in grid_model if t.type in "Trafo"]
    ext_grid = [e for e in grid_model if e.type in "Ext_grid"]

    # Connect entities - mosaik_models to PP_entities #####################
    for i in range(len(houseLoad)):
        # world.connect(csv_load_model[0],
        #               houses_model[i], (f'House{i}', 'p_in'))
        world.connect(houses_model[i], houseLoad[i], ("P", "p_mw"))

        if pv:
            if STATUS == "run":
                world.connect(
                    meteo_model[0],
                    pv_model[i],
                    ("GlobalRadiation", "GHI"),
                    ("AirPressHourly", "pressure"),
                    ("AirTemperature", "airTemp"),
                    ("WindSpeed", "windSpeed"),
                )

                world.connect(pv_model[i], pv_gens[i], ("P", "p_mw"))

            if STATUS == "test" or STATUS == "run_prognosis":
                world.connect(pv_model[i], pv_gens[i], ("P", "p_mw"))

        if storage:
            for i in range(len(storageElements)):
                world.connect(
                    storages[i],
                    storageElements[i],
                    ("P", "p_mw"),
                    ("P_FLEX_CHARGE_MAX", "max_p_mw"),
                    ("P_FLEX_DISCHARGE_MAX", "min_p_mw"),
                )

        if cs:
            for i in range(N_CARS):
                world.connect(cs[i], csLoads[i], ("P", "p_mw"), async_requests=True)

    # Add Buses and grid to GridObserver
    if grid_obs:
        grid_obs_sim.add_controlled_buses(buses)
        grid_obs_sim.add_controlled_grid(grid_parent)
        # grid_corr_sim.add_controlled_grid(grid_parent)
        world.connect(ext_grid[0], grid_obs_model[0], ("p_mw", "ext_grid_p_mw"))
        world.connect(grid_obs_model[0], grid_corr_model[0], "net", "status")

        for i in range(49):
            world.connect(grid_corr_model[0], grid_node_model[i], ("b_p_inj", "list_input"))

    # Add components to EMS
    if control and cs:
        keys = [k for k in csEntities.keys()]

        items = math.ceil(N_CARS / 49)  # 1 for 1-49, 2 for 50-98, etc,
        cs_entities_split = {}
        for i in range(items):
            cs_entities_split[i] = {k: csEntities[k] for k in keys[49 * i : 49 * (i + 1)]}
            control_sim.add_controlled_entity(BEM_control_entities, cs_entities_split[i])

        control_sim.add_controlled_entity(BEM_control_entities, pv_model_entities)
        control_sim.add_controlled_entity(BEM_control_entities, housesEntities)

    if control and storage:
        control_sim.add_controlled_entity(BEM_control_entities, storagesEntities)

    # connect models to respective control
    if control:
        for i in range(49):
            world.connect(
                houses_model[i],
                BEM[i],
                ("P", "P_LOAD"),
                ("Q", "Q_LOAD"),
                "SIGNAL_PROGNOSIS",
                "PROGNOSIS_LOAD",
                async_requests=True,
            )
            world.connect(
                pv_model[i],
                BEM[i],
                ("P", "P_PV"),
                "SIGNAL_PROGNOSIS",
                "PROGNOSIS_PV",
                async_requests=True,
            )

        if cs:
            for i in range(N_CARS):
                world.connect(
                    cs[i],
                    BEM[i % 49],
                    ("P_MIN", "P_MIN"),
                    ("P_MAX", "P_MAX"),
                    ("P_SALDO_RES", "P_SALDO_RES"),
                    ("STANDING_TIME_START", "PROGNOSIS_START"),
                    ("STANDING_TIME_END", "PROGNOSIS_END"),
                    ("RES_SIGNAL", "RES_SIGNAL"),
                    "SIGNAL_PROGNOSIS_CS_control",
                    async_requests=True,
                )

        if storage:
            for i in range(len(houseLoad)):
                pass
                world.connect(
                    storages[i],
                    BEM[i],
                    ("P_DISCHARGE_MAX", "P_MIN"),
                    ("P_CHARGE_MAX", "P_MAX"),
                    async_requests=True,
                )

    # Connect Database elements
    connect_many_to_one(
        world, houses_model, database_model, "P", "PROGNOSIS_LOAD", "SIGNAL_PROGNOSIS"
    )
    connect_many_to_one(world, houseLoad, database_model, "p_mw")
    connect_many_to_one(world, buses, database_model, "p_mw", "q_mvar", "vm_pu", "va_degree")
    connect_many_to_one(world, trafo, database_model, "loading_percent", "tap_pos")
    if pv and STATUS == "run":
        connect_many_to_one(
            world,
            meteo_model,
            database_model,
            "GlobalRadiation",
            "AirPressHourly",
            "AirTemperature",
            "WindSpeed",
        )

    if pv:
        connect_many_to_one(world, pv_model, database_model, "P")

    if grid_obs:
        connect_many_to_one(world, grid_obs_model, database_model, "status")
        connect_many_to_one(world, grid_node_model, database_model, "p_inj", "vm_pu")

    if control:
        connect_many_to_one(world, BEM, database_model, "P_SALDO")

    if storage:
        connect_many_to_one(
            world,
            storages,
            database_model,
            "SOC",
            "P",
            "P_SET",
            "P_CHARGE_MAX",
            "P_DISCHARGE_MAX",
        )

        connect_many_to_one(
            world,
            storageElements,
            database_model,
            "p_mw",  # active power [MW]
            "max_e_mwh",  # maximum energy content of the storage [MWh]
            "q_mvar",  # reactive power [MVAr]
            "soc_percent",  # state of charge of the storage
            "in_service",  # specifies if the load is in service.
            # 'controllable',# States if the storage unit is controllable or not.
            "min_p_mw",  # Minimum power (negative) for discharging
            "max_p_mw",  # Maximum power (positive) for charging
        )

    if cs and car:
        connect_many_to_one(
            world, cs, database_model, "P", "SIGNAL_PROGNOSIS_CS_control", "PROGNOSIS_RESIDUAL"
        )
        connect_many_to_one(
            world,
            cars,
            database_model,
            "appearance",
            "SOC",
            "SIGNAL_PROGNOSIS_EV_CS",
            "E_BAT",
            "E_BAT_MAX",
        )

    return world


def run_scenario(configuration, scenario=Scenarios.TEST):
    """
    Chain of methods to fully run an scenario
    :param configuration: Dictionary with configuration parameters
    :param scenario: Scenario(Enum) category

    Example::

        configuration = {
            "start": '2020-01-01 00:00:00',
            "end": 24 * 3600 * 2,
            "step_size": 60 * 15,
            "overwrite_data": False}

        run_scenario(scenario=Scenarios.SIMPLE,
                     configuration=configuration)

    """
    world = create_world(configuration)
    world = configure_scenario(world, configuration, scenario)
    END = configuration["end"]
    world.run(until=END, print_progress=True)
    try:
        analyze_database(scenario=scenario, configuration=configuration)
        logging.info(f"Database results plotted in {RESULTS_POSTPROC_DIR}")
    except:
        logging.info(f"{RESULTS_POSTPROC_DIR} could not be analyzed")

    logging.info("Model calculated")
    logging.info("-------------------------------------------------------------\n")


if __name__ == "__main__":
    # This is the main configuration to control the simulation parameters

    # "test" uses CSV of PV for faster simulation.
    configuration = {
        "start": "2020-03-11 00:00:00",
        "end": 24 * 3600 * 15,
        "step_size": 60 * 15,
        "overwrite_data": False,
        "n_cars": 49 * 4,
        "soc_lims": {"soc_min": 0.2, "soc_max": 0.8},
        "status": "test",
    }  # "test", "run"

    # The scenarios control the structure of the model
    run_scenario(scenario=Scenarios.GRID_OBSV, configuration=configuration)
