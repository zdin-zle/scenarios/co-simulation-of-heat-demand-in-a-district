'''
@authors:   Fernando Penaherrera @UOL/OFFIS
            Henrik Wagner elenia@TUBS
'''
import os
path = os.path.realpath(os.path.join(os.path.dirname(__file__), os.pardir))

SIM_CONFIG = {
    'CarSim': {
        'python': 'src.components.car.car_simulator:Sim',
    },
    'CSSim': {
        'python': 'src.components.charging_station.charging_station_simulator:Sim'
    },
    'ControlSim' :{
        'python': 'src.components.control.control_simulator:Sim'
    },
    'mosaik-CSV': {
        'python': 'src.components.mosaik-csv_reader.mosaik-csv_simulator:CSV'
    },
    'Load': {
        'python': 'src.components.csv_reader.csv_reader_simulator:Sim'
    },
    'DB': {
        'python': 'mosaik_hdf5:MosaikHdf5',
    },
    'Pandapower': {
        'python': 'src.components.grid.simulator:Pandapower'
    },
    'PV': {
        'python': 'src.components.csv_reader.csv_reader_simulator:Sim'
    },  
    'PVSim': {
        'python': 'src.components.pv.photovoltaic_simulator:PVSimulator'
    },
    'StorageSim':{
        'python': 'src.components.storage.storage_simulator:StorageSimulator'
    },
    'ControlSim' :{
        'python': 'src.components.control.control_simulator:Sim'
    },
    'GridObsSim' :{
        'python': 'src.components.grid_observer.grid_observer_simulator:Sim'
    },
    'GridCorrSim' :{
        'python': 'src.components.grid_corrector.grid_corrector_simulator:Sim'
    },
    'GridNodeSim' :{
        'python': 'src.components.grid_corrector.grid_node_simulator:Sim'
    },
}

