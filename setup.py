from setuptools import setup, find_packages

setup(
   name='PackageName',
   version='0.1.0',
   author='An Awesome Coder',
   author_email='aac@example.com',
   
   # The following statements indicate that for installation as package, ONLY the contents of the
   # folder scenario* under the folder "src" are going to be installed. All the other components 
   # will be ignored  
   packages=find_packages(
        where="src",
        include=["scenario*"]
    ),
    package_dir={"": "src"},
    
    
    
   scripts=['bin/script1','bin/script2'],
   url='http://pypi.python.org/pypi/PackageName/',
   license='LICENSE.txt',
   description='An awesome package that does something',
   long_description=open('README.txt').read(),
   install_requires=[
       "Django >= 1.1.1",
       "pytest",
   ],
)